import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux';
import { ToastContainer } from "react-toastify";
import store from './store';
import 'react-toastify/dist/ReactToastify.css';

import './css/sb-admin.min.css';
import './css/custom.css';
import CheckLogin from "./CheckLogin";

function App() {
    return(
       <Provider store={store}>
          <BrowserRouter>
            <ToastContainer
               position="top-right"
               autoClose={5000}
               hideProgressBar={false}
               newestOnTop={false}
               closeOnClick
               rtl={false}
               pauseOnFocusLoss
               draggable
               pauseOnHover
            />
            <CheckLogin />
          </BrowserRouter>
       </Provider>
    );
}

export default App;