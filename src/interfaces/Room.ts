export interface IRoom {
    id: number,
    owner_id: string,
    category: number,
    name: string,
    description: string,
    model: string,
    ccts: string,
    wallpaper: number,
    floor: number,
    showname: boolean,
    superusers: boolean,
    visitors_max: number,
    visitors_now: number,
    rating: number,
    password: string,
    accesstype: number
}

export interface IRoomCategory {
    id: number,
    order_id: number,
    parent_id: number,
    isnode: number,
    name: string,
    public_spaces: number,
    allow_trading: number,
    minrole_access: number,
    minrole_setflatcat: number,
}

export interface IRoomRight {
    user_id: number,
    room_id: number
}

export interface IRoomLog {
    user_id: number,
    room_id: number,
    timestamp: number,
    chat_type: number,
    message: string
}