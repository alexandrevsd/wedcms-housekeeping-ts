export interface ICataloguePage {
    id: number,
    order_id: number,
    min_role: number,
    index_visible: boolean,
    is_club_only: boolean,
    name_index: string,
    link_list: string,
    name: string,
    layout: string,
    image_headline: string,
    image_teasers: string,
    body: string,
    label_pick: string,
    label_extra_s: string,
    label_extra_t: string
}