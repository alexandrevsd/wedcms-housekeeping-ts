export interface IVoucherHistory {
    voucher_code: string,
    user_id: string,
    used_at: Date,
    credits_redeemed: number,
    items_redeemed: string
}