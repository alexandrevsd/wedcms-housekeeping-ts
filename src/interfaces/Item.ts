export interface IItem {
    id: number,
    order_id: number,
    user_id: number,
    room_id: number,
    definition_id: number,
    x: string,
    y: string,
    z: string,
    wall_position: string,
    rotation: number,
    custom_data: string,
    is_hidden: boolean,
    created_at: Date,
    updated_at: Date
}

export interface IItemDefinition {
    id: number,
    sprite: string,
    sprite_id: number,
    name: string,
    description: string,
    colour: string,
    length: number,
    width: number,
    top_height: number,
    max_status: number,
    behaviour: string,
    interactor: string,
    is_tradable: boolean,
    is_recyclable: boolean,
    drink_ids: string
}

export interface IItemImage {
    id: number,
    item_definition_id: number,
    little_image: string
}