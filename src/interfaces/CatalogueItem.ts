export interface ICatalogueItem {
    id: number,
    sale_code: string,
    page_id: string,
    order_id: number,
    price: number,
    is_hidden: boolean,
    amount: number,
    definition_id: number,
    item_specialspriteid: number,
    name: string,
    description: string,
    is_package: boolean,
    package_name: string,
    package_description: string
}

export interface IPackage {
    id: number,
    salecode: string,
    definition_id: number,
    special_sprite_id: number,
    amount: number
}

export interface ICataloguePackage {
    id: number,
    salecode: string,
    definition_id: number,
    special_sprite_id: number,
    amount: number
}