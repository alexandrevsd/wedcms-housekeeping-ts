export interface IUser {
    id: number,
    username: string,
    password?: string,
    figure: string,
    pool_figure: string,
    sex: string,
    motto: string,
    credits: number,
    tickets: number,
    film: number,
    rank: number,
    console_motto: string,
    last_online: number,
    created_at: Date,
    updated_at: Date,
    sso_ticket: string,
    club_subscribed: number,
    club_expiration: number,
    club_gift_due: number,
    badge: string,
    badge_active: boolean,
    allow_stalking: boolean,
    allow_friends_requests: boolean,
    sound_enabled: boolean,
    tutorial_finished: boolean,
    battleball_points: number,
    snowstorm_points: number
}

export interface IUserItem {
    user: IUser,
    index: number
}

export interface IUserIpLog {
    user_id: number,
    ip_address: string,
    created_at: Date
}

export interface IUserBan {
    ban_type: string,
    banned_value: string,
    message: string,
    banned_until: number
}

export interface IUserBadge {
    user_id: number,
    badge: string
}