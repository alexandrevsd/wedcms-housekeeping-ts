import axios, {AxiosInstance, AxiosResponse} from 'axios';
import { toast } from 'react-toastify';
import { Dispatch } from 'redux';
import { setLogoutAction } from './store/userActions';




class Api {

    private static API_URL: string = '/api/';
    private static api: AxiosInstance;

    private static getApi(): AxiosInstance {
        if(Api.api === undefined) {
            Api.api = axios.create({
                baseURL: Api.API_URL,
                withCredentials: true
            });
        }
        return Api.api;
    }

    static checkLogged(res: any, dispatch: Dispatch): boolean {
        let isLogged: boolean = true;
        if(res.logged === false) {
            dispatch(setLogoutAction());
            toast.error("Tu as été déconnecté !");
            isLogged = false;
        }
        return isLogged;
    }

    static async get(url: string) {
        const response: AxiosResponse = await Api.getApi().get(url);
        return response.data;
    }

    static async getFrom(url: string) {
        const axiosInstance: AxiosInstance = axios.create({withCredentials: true});
        const response: AxiosResponse = await axiosInstance.get(url);
        return response;
    }

    static async post(url: string, body: object) {
        const response: AxiosResponse = await Api.getApi().post(url, body);
        return response.data;
    }

    static async delete(url: string) {
        const response: AxiosResponse = await Api.getApi().delete(url);
        return response.data;
    }

}

export default Api;