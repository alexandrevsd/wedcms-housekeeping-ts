import { useState } from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import Api from "../../../Api";
import { IRoom, IRoomRight } from "../../../interfaces/Room";
import { IUser } from "../../../interfaces/User";
import Input from "../../main/form/Input";
import Select from "../../main/form/Select";
import SubmitButton from "../../main/form/SubmitButton";
import PageBlock from "../../main/pageContent/PageBlock";

export default function Rights(props: any) {

    const room: IRoom = props.room;
    const dispatch = useDispatch();

    const [users, setUsers] = useState<Array<IUser>>([]);
    const [inputUsername, setInputUsername] = useState<string>('');
    const [inputRemoveUsername, setInputRemoveUsername] = useState<string>('');

    const handleInputUsername = (e: any) => setInputUsername(e.target.value);
    const handleInputRemoveUsername = (e: any) => setInputRemoveUsername(e.target.value);

    useEffect(() => {
        loadRightUsers();
    }, []);

    const loadRightUsers = async () => {
        const res = await Api.get('room/rights?id=' + room.id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.users) {
            setUsers(res.users);
            res.users.length > 0 && setInputRemoveUsername(res.users[0].id);
        }
    }

    const handleButtonSubmitAddRight = async (e: any) => {
        e.preventDefault();
        if(inputUsername !== '') {
            const res = await Api.post('room/right', {username: inputUsername, roomId: room.id});
            const logged = Api.checkLogged(res, dispatch);
            if(logged && res.response === 200) {
                toast.success("Les droits ont bien été ajoutés à l'utilisateur.")
                loadRightUsers();
            } else if(res.response === 400) {
                switch (res.error) {
                    case 'userDoesntExists':
                        toast.error("L'utilisateur n'existe pas.");
                        break;
                    case 'userAlreadyHaveRights':
                        toast.error("L'utilisateur possède déjà les droits.");
                        break;
                }
            }
        }
    }

    const handleButtonSubmitRemoveRight = async (e: any) => {
        e.preventDefault();        
        if(inputRemoveUsername !== '') {
            const res = await Api.delete('room/right?userId=' + inputRemoveUsername + '&roomId=' + room.id,);
            const logged = Api.checkLogged(res, dispatch);
            if(logged && res.response === 200) {
                toast.success("Les droits ont bien été retirés à l'utilisateur.")
                loadRightUsers();
            }
        }
    }

    return (
        <PageBlock title="Droits pour l'appartement">
            <form>
                <Input label="Nom d'utilisateur à qui ajouter des droits" value={inputUsername} onChange={handleInputUsername} />
                <SubmitButton color="success" onClick={handleButtonSubmitAddRight}>Ajouter le droit</SubmitButton>
            </form>
            {users && users.length > 0 ?
                <form>
                    <Select value={inputRemoveUsername} onChange={handleInputRemoveUsername} label="Utilisateurs ayant les droits">
                        {users.map((user: IUser) => {
                            return <option value={user.id} key={user.id}>{user.username}</option>
                        })}
                    </Select>
                    <SubmitButton color="danger" onClick={handleButtonSubmitRemoveRight}>Retirer le droit</SubmitButton>
                </form>
                :
                <p>Personne ne possède de droits dans cet appartement.</p>
            }
        </PageBlock>
    );
}