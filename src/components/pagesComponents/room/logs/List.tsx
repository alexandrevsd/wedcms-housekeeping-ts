import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Api from "../../../../Api";
import { IRoomLog } from "../../../../interfaces/Room";
import { IUser } from "../../../../interfaces/User";
import PageBlock from "../../../main/pageContent/PageBlock";

export default function List(props: any) {

    const { room, page, setPage, setPrevious, setNext, setMaxPage } = props;

    const dispatch = useDispatch();

    const [loading, setLoading] = useState<boolean>(true);
    const [logs, setLogs] = useState<Array<IRoomLog>>([]);
    const [users, setUsers] = useState<Map<number, IUser>>(new Map());

    useEffect(() => {
        loadRoomLogs();
    }, [page]);

    const loadRoomLogs = async () => {
        const res = await Api.get('room/logs?id=' + room.id + '&page=' + page);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            setPage(res.page);
            setMaxPage(res.maxPage);
            setNext(res.next);
            setPrevious(res.previous);
            setLogs(res.logs);
            const newUsers = new Map();
            res.users.forEach((user: IUser) => {
                newUsers.set(user.id, user);
            })
            setUsers(newUsers);
        }
        setLoading(false);
    }

    return (
        <PageBlock>
            {logs && logs.length > 0 ?
                <>
                    <table className="table table-hover table-dark mt-3">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Date</th>
                                <th>Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            {logs.map((log, i) => {
                                const user = users.get(log.user_id)
                                const username = user && user.username;
                                return <tr key={i}>
                                    <td>{username}</td>
                                    <td>{new Date(log.timestamp * 1000).toLocaleString()}</td>
                                    <td>{log.message}</td>
                                </tr>;
                            })}
                        </tbody>
                    </table>
                </>
                :
                loading ?
                    <p>Chargement...</p>
                    :
                    <p className="text-center" style={{ fontSize: '1.5em' }}>Il n'y a pas de logs pour cet appartement</p>
            }
        </PageBlock>
    );
}