import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import Api from "../../../../Api";
import useModal from "../../../../hooks/useModal";
import { IItem, IItemDefinition, IItemImage } from "../../../../interfaces/Item";
import ActionButton from "../../../main/buttons/ActionButton";
import Button from "../../../main/buttons/Button";
import Modal from "../../../main/Modal";
import PageBlock from "../../../main/pageContent/PageBlock";

export default function List(props: any) {

    const { room, page, setPage, setPrevious, setNext, setMaxPage } = props;

    const dispatch = useDispatch();

    const { isShowing, toggle: toggleConfirm } = useModal();
    const [modalContent, setModalContent] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(true);
    const [items, setItems] = useState<Array<IItem>>();
    const [itemDefinitions, setItemDefinitions] = useState<Array<IItemDefinition>>();
    const [itemImages, setItemImages] = useState<Array<IItemImage>>();

    useEffect(() => {
        loadRoomItems();
    }, [page]);

    const loadRoomItems = async () => {
        const res = await Api.get('room/items?id=' + room.id + '&page=' + page);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            setItemImages(res.itemImages);
            setItemDefinitions(res.itemDefinitions);
            setItems(res.items);
            setPage(parseInt(res.page));
            setPrevious(res.previous);
            setNext(res.next);
            setMaxPage(res.maxPage);
        }
        setLoading(false);
    }

    const handleButtonDeleteItem = async (id: string) => {
        const res = await Api.delete('item?id=' + id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            toast.success("Le mobis a bien été supprimé, un redémarrage du serveur doit être effectué.")
            loadRoomItems();
        }
    }

    const handleButtonEmptyRoomConfirm = (e: any) => {
        e.preventDefault();
        setModalContent("Voulez-vous vraiment vider l'appartement \"" + room.name + "\" ?")
        toggleConfirm();
    }

    const handleButtonEmptyRoom = async () => {
        toggleConfirm();
        const res = await Api.delete('room/items?id=' + room.id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            toast.success("L'appartement a bien été vidé, un redémarrage du serveur doit être effectué.")
            loadRoomItems();
        }
    }

    return (
        <PageBlock>
            {items && itemDefinitions && itemImages && items.length > 0 ?
                <>
                    <Modal
                        title="Attention !"
                        content={modalContent}
                        isShowing={isShowing}
                        onClick={handleButtonEmptyRoom}
                        yesButton="Oui"
                        noButton="Annuler"
                        hide={toggleConfirm}
                    />
                    <Button color="danger" onClick={handleButtonEmptyRoomConfirm}>Vider l'appartement</Button>
                    <table className="table table-hover table-dark mt-3">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Nom</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {items?.map((item, i) => {
                                return <tr key={i}>
                                    <td>{itemImages[i] && <img src={'/images/little-furni/' + itemImages[i].little_image + '.gif'} />}</td>
                                    <td>{itemDefinitions[i] && itemDefinitions[i].name}</td>
                                    <td>
                                        <ActionButton onClick={() => handleButtonDeleteItem(item.id.toString())} color="danger" icon="trash" title="Supprimer l'objet" />
                                    </td>
                                </tr>;
                            })}
                        </tbody>
                    </table>
                </>
                :
                loading ?
                    <p>Chargement...</p>
                    :
                    <p className="text-center" style={{ fontSize: '1.5em' }}>Il n'y a pas d'objets dans cet appartement</p>
            }
        </PageBlock>
    );
}