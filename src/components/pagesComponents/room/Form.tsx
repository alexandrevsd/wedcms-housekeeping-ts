import { useEffect } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import Api from "../../../Api";
import { IRoom, IRoomCategory } from "../../../interfaces/Room";
import Input from "../../main/form/Input";
import Select from "../../main/form/Select";
import SubmitButton from "../../main/form/SubmitButton";
import PageBlock from "../../main/pageContent/PageBlock";

export default function Form(props: any) {

    const room: IRoom = props.room;
    const dispatch = useDispatch();


    const [categories, setCategories] = useState<Array<IRoomCategory>>([]);
    const [inputName, setInputName] = useState<string>(room.name);
    const [inputDescription, setInputDescription] = useState<string>(room.description);
    const [inputCategory, setInputCategory] = useState<string>(room.category.toString());
    const [inputVisitorsMax, setInputVisitorsMax] = useState<string>(room.visitors_max.toString());
    const [inputAccessType, setInputAccessType] = useState<string>(room.accesstype.toString());
    const [inputPassword, setInputPassword] = useState<string>(room.password);

    const handleInputName = (e: any) => setInputName(e.target.value);
    const handleInputDescription = (e: any) => setInputDescription(e.target.value);
    const handleInputCategory = (e: any) => setInputCategory(e.target.value);
    const handleInputVisitorsMax = (e: any) => setInputVisitorsMax(e.target.value);
    const handleInputAccessType = (e: any) => setInputAccessType(e.target.value);
    const handleInputPassword = (e: any) => setInputPassword(e.target.value);

    useEffect(() => {
        loadCategories();
    }, []);

    const loadCategories = async () => {
        const res = await Api.get('rooms/categories');
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.categories) {
            setCategories(res.categories);
        }
    }

    const handleSubmitRoom = async () => {
        const edittedRoom = {
            id: room.id,
            name: inputName,
            description: inputDescription,
            category: inputCategory,
            visitors_max: inputVisitorsMax,
            accesstype: inputAccessType,
            password: inputPassword
        }
        const res = await Api.post('room', {room: edittedRoom});
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.response === 200) {
            toast.success("L'appartement a bien été modifié.")
        } else if(res.response === 400) {
            toast.error("Tu n'as pas la permission d'utiliser cette catégorie.")
        }
    }

    return (
        <PageBlock>
            <Input onChange={handleInputName} value={inputName} label="Nom" type="text" id="name" />
            <Input onChange={handleInputDescription} value={inputDescription} label="Description" type="text" id="description" />
            <Select onChange={handleInputCategory} value={inputCategory} label="Catégorie">
                {categories.map((category: IRoomCategory) => {
                    return <option value={category.id} key={category.id}>{category.name}</option>
                })}
            </Select>
            <Select onChange={handleInputVisitorsMax} value={inputVisitorsMax} label="Visiteurs maximum">
                <option value="25">25</option>
                <option value="20">20</option>
                <option value="15">15</option>
                <option value="10">10</option>
            </Select>
            <Select onChange={handleInputAccessType} value={inputAccessType} label="Type d'accès">
                <option value="0">Ouvert</option>
                <option value="1">Fermé</option>
                <option value="2">Mot de passe</option>
            </Select>
            {inputAccessType === '2' && <Input advice="Laisser tel qu'il est pour ne pas le changer" onChange={handleInputPassword} value={inputPassword} label="Mot de passe" type="text" id="password" />}
            <SubmitButton onClick={handleSubmitRoom} color="primary">Sauvegarder</SubmitButton>
        </PageBlock>
    );
}