import { IRoom } from "../../../interfaces/Room";

export default function Informations(props: any) {

    const room: IRoom = props.room;

    return (
        <div className="wd-room-infos">
            <img src={"/images/rooms-models/" + room.model + ".png"} />
            <div>
                <p className="mb-0">En ligne : {room.visitors_now}</p>
                <p>Note : {room.rating}</p>
            </div>
        </div>
    );
}