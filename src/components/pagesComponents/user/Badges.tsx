import PageBlock from "../../main/pageContent/PageBlock";
import ScrollableBox, { useDefaultLipClassNames } from 'react-scrollable-box';
import 'react-scrollable-box/lib/default.css';
import { useEffect } from "react";
import Api from "../../../Api";
import { useDispatch } from "react-redux";
import '../../../css/imagepicker.css'
import { useState } from "react";
import ImagePicker, { IImagePickerImage } from "../../main/form/ImagePicker";
import Button from "../../main/buttons/Button";
import { IUserBadge } from "../../../interfaces/User";
import { toast } from "react-toastify";

export default function Badges(props: any) {

    const {user} = props;
    const dispatch = useDispatch();
    const lipClassNames = useDefaultLipClassNames();

    const [badges, setBadges] = useState<Array<string>>([]);
    const [selectedBadges, setSelectedBadges] = useState<Array<string>>([]);

    useEffect(() => {
        loadBadges();
    }, []);

    const loadBadges = async () => {
        const res = await Api.get('user/badges?id=' + user.id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.badges && res.userBadges) {
            setBadges(res.badges);
            const userBadges: Array<string> = [];
            res.userBadges.forEach((badge: IUserBadge) => {
                userBadges.push(badge.badge);
            })            
            setSelectedBadges(userBadges);
        }
    }

    const handlePickImage = (images: Array<string>) => {
        setSelectedBadges(images);
    }

    const handleSubmitBadges = async (e: any) => {
        e.preventDefault();
        const res = await Api.post('user/badges', {userId: user.id, badges: selectedBadges});
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.response === 200) {
            toast.success("Les badges de l'utilisateurs ont bien été sauvegardés.")
        }
    }

    return (
        <PageBlock title="Badges">
            <ScrollableBox
                {...lipClassNames}
                style={{ maxHeight: '240px', overflow: 'auto' }}
            >
                <ImagePicker
                    selected={selectedBadges}
                    onSelect={handlePickImage}
                    images={badges.map((badge, i) => ({
                        src: '/v14/c_images/badges/wedcms/' + badge + '.gif',
                        value: badge
                    }))}
                />

            </ScrollableBox>
            <div className="mt-3">
                <Button color="primary" onClick={handleSubmitBadges}>Sauvegarder</Button>
            </div>
        </PageBlock>
    );
}