import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Api from "../../../Api";
import { IUserIpLog } from "../../../interfaces/User";
import PageBlock from "../../main/pageContent/PageBlock";

export default function Ips(props: any) {

    const { user, setIp } = props;

    const dispatch = useDispatch();
    const [ipLogs, setIplogs] = useState<Array<IUserIpLog>>();


    useEffect(() => {
        loadIpLogs();
    }, []);

    const loadIpLogs = async () => {
        const res = await Api.get('user/ipLogs?id=' + user.id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.ipLogs) {
            setIplogs(res.ipLogs);
            res.ipLogs[0] && setIp(res.ipLogs[0].ip_address);
        }
    }

    return (
        <PageBlock title="20 dernières connexions">
            {ipLogs && ipLogs.length > 0 ?
                <table className="table table-hover table-dark">
                    <thead>
                        <tr>
                            <th>Adresse IP</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ipLogs.map((ipLog, i) => {
                            return (
                                <tr key={i}>
                                    <td>{ipLog.ip_address}</td>
                                    <td>{new Date(ipLog.created_at).toLocaleString()}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                :
                <p className="text-center" style={{ fontSize: '1.5em' }}>{user.username} ne s'est jamais connecté</p>
            }
        </PageBlock>
    );
}