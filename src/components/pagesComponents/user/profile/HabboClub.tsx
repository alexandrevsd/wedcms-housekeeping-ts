import { useDispatch } from "react-redux";
import Api from "../../../../Api";
import { IUser } from "../../../../interfaces/User";
import Button from "../../../main/buttons/Button";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export default function HabboClub(props: any) {

    const dispatch = useDispatch();

    const {user, setUser, hcMember} = props;

    const handleButtonAddHcMonths = async (e: any, months: number) => {
        e.preventDefault();
        e.target.disabled = true;
        const res = await Api.post('misc/hcMonths', { id: user?.id, months });
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.club_expiration && user) {
            const newUser: IUser = { ...user };
            newUser.club_expiration = res.club_expiration;
            setUser(newUser);
            if(months > 1) {
                toast.success(months + " mois HC ont été ajouté à " + user.username + ".");
            } else {
                toast.success("1 mois HC a été ajouté à " + user.username + ".");
            }
        }
        e.target.disabled = false;
    }

    const handleButtonRemoveHc = async (e: any) => {
        e.preventDefault();
        e.target.disabled = true;
        if (user) {
            const res = await Api.delete('user/hc?id=' + user.id);
            const logged = Api.checkLogged(res, dispatch);
            if (logged && res.response === 200) {
                const newUser: IUser = { ...user };
                newUser.club_expiration = 0;
                setUser(newUser);
                toast.success(user.username + " n'est plus HC.");
            }
        }
        e.target.disabled = false;
    }

    return (
        <div className="mb-3">
            <label>Ajouter du temps HC</label>
            <div className="row">
                <div className="col-sm">
                    <Button onClick={(e: any) => handleButtonAddHcMonths(e, 1)} color="success">1 mois</Button>
                </div>
                <div className="col-sm">
                    <Button onClick={(e: any) => handleButtonAddHcMonths(e, 3)} color="success">3 mois</Button>
                </div>
                <div className="col-sm">
                    <Button onClick={(e: any) => handleButtonAddHcMonths(e, 6)} color="success">6 mois</Button>
                </div>
                <div className="col-sm">
                    <Button onClick={handleButtonRemoveHc} color="danger">Tout retirer</Button>
                </div>
            </div>
            {hcMember && <small className="form-text text-muted">{user.username} est actuellement membre HC jusqu'au {new Date(user.club_expiration * 1000).toLocaleString()}</small>}
            {!hcMember && <small className="form-text text-muted">{user.username} n'est actuellement pas membre HC</small>}
        </div>
    );
}