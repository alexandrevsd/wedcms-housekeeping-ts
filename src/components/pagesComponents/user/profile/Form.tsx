import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Api from "../../../../Api";
import { userSelectors } from "../../../../store/userSelectors";
import Input from "../../../main/form/Input";
import Select from "../../../main/form/Select";
import SubmitButton from "../../../main/form/SubmitButton";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export default function Form(props: any) {

    const userState = useSelector(userSelectors);
    const dispatch = useDispatch();

    const { user, setUser } = props;

    const [inputRank, setInputRank] = useState<string>('');
    const [inputPassword, setInputPassword] = useState<string>('');
    const [inputMotto, setInputMotto] = useState<string>('');
    const [inputConsoleMotto, setInputConsoleMotto] = useState<string>('');
    const [inputCredits, setInputCredits] = useState<number>(0);
    const [inputTickets, setInputTickets] = useState<number>(0);
    const [inputFilms, setInputFilms] = useState<number>(0);
    const [inputBattleballPoints, setInputBattleballPoints] = useState<number>(0);
    const [inputSnowstormPoints, setInputSnowstormPoints] = useState<number>(0);

    useEffect(() => {
        setInputRank(user.rank);
        setInputMotto(user.motto);
        setInputConsoleMotto(user.console_motto);
        setInputCredits(user.credits);
        setInputTickets(user.tickets);
        setInputFilms(user.film);
        setInputBattleballPoints(user.battleball_points);
        setInputSnowstormPoints(user.snowstorm_points);
    }, [user]);

    const handleInputPassword = (e: any) => setInputPassword(e.target.value);
    const handleInputRank = (e: any) => setInputRank(e.target.value);
    const handleInputMotto = (e: any) => setInputMotto(e.target.value);
    const handleInputConsoleMotto = (e: any) => setInputConsoleMotto(e.target.value);
    const handleInputCredits = (e: any) => setInputCredits(e.target.value);
    const handleInputTickets = (e: any) => setInputTickets(e.target.value);
    const handleInputFilms = (e: any) => setInputFilms(e.target.value);
    const handleInputBattleballPoints = (e: any) => setInputBattleballPoints(e.target.value);
    const handleInputSnowstormPoints = (e: any) => setInputSnowstormPoints(e.target.value);

    const handleSubmitUser = async (e: any) => {
        e.preventDefault();
        e.target.disabled = true;
        const editedUser = { ...user };
        editedUser.password = inputPassword;
        editedUser.rank = parseInt(inputRank);
        editedUser.motto = inputMotto;
        editedUser.console_motto = inputConsoleMotto;
        editedUser.credits = inputCredits;
        editedUser.tickets = inputTickets;
        editedUser.film = inputFilms;
        editedUser.battleball_points = inputBattleballPoints;
        editedUser.snowstorm_points = inputSnowstormPoints;
        const res = await Api.post('user', { user: editedUser });
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            setUser(editedUser);
            toast.success("Toutes les informations ont été enregistrées.");
        } else if(res.response === 400) {
            let message: string | undefined = undefined;
            switch(res.error) {
                case 'notAllowed':
                    message = "Tu ne peux pas séléctionner un rang plus haut que le tiens.";
                    break;
                case 'userDoesntExists':
                    message = "L'utilisateur n'existes pas."
                    break;
            }
            if(message) toast.error(message);
        }
        e.target.disabled = false;
    }

    const selectOptions: Array<JSX.Element> = [];
    userState.rankNames.forEach((name: string, id: number) => {
        selectOptions.push(<option value={id} key={id}>{name}</option>)
    })

    return (
        <form>
            <Input label="Mot de passe" advice="Laisser vide pour ne pas changer" onChange={handleInputPassword} value={inputPassword} id="password" type="password" />
            <Select label="Rang" value={inputRank} onChange={handleInputRank}>
                {selectOptions}
            </Select>
            <Input label="Mission" onChange={handleInputMotto} value={inputMotto} id="motto" type="text" />
            <Input label="Mission de la console" onChange={handleInputConsoleMotto} value={inputConsoleMotto} id="console_motto" type="text" />
            <Input label="Crédits" onChange={handleInputCredits} value={inputCredits} id="credits" type="number" />
            <Input label="Tickets" onChange={handleInputTickets} value={inputTickets} id="tickets" type="number" />
            <Input label="Pellicules photo" onChange={handleInputFilms} value={inputFilms} id="films" type="number" />
            <Input label="Points Battleball" onChange={handleInputBattleballPoints} value={inputBattleballPoints} id="battleball_points" type="number" />
            <Input label="Points Snowstorm" onChange={handleInputSnowstormPoints} value={inputSnowstormPoints} id="snowstorm_points" type="number" />
            <SubmitButton color="primary" onClick={handleSubmitUser}>Sauvegarder</SubmitButton>
        </form>
    );
}