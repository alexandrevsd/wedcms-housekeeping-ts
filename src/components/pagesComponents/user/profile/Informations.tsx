export default function Informations(props: any) {

    const {hcMember, banned, ip, user} = props;

    return (
        <>
            <div className="habbo-profile-container mb-3">
                <div className="habbo-profile">
                    <div className="habbo-profile-infos">
                        <div>
                            {user.last_online !== 0 && new Date(user.last_online * 1000).toLocaleString()}<br />
                            {ip}
                        </div>
                        {banned ?
                            <i className={"fas fa-fw fa-ban text-danger ml-2"} title="Utilisateur banni" />
                            :
                            hcMember &&
                            <div>
                                <img src="/v14/c_images/badges/wedcms/HC1.gif" />
                            </div>
                        }
                    </div>
                    <div className="habbo-profile-image">
                        <img src={"/habbo-imager/?figure=" + user.figure + "&little=true&reverse=true"} />
                        {user.badge && <div className="habbo-profile-badge">
                            <img src={"/v14/c_images/badges/wedcms/" + user.badge + ".gif"} />
                        </div>}
                    </div>
                    <div className="habbo-profile-name">
                        {user.username}
                    </div>
                </div>
            </div>
            <div style={{ clear: 'both' }} />
        </>
    );
}