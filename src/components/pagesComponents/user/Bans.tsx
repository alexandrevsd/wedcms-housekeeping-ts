import { useEffect, useState } from "react";
import Api from "../../../Api";
import { IUserBan } from "../../../interfaces/User";
import PageBlock from "../../main/pageContent/PageBlock";

export default function Bans(props: any) {

    const user = props.user;
    const [bansList, setBansList] = useState<Array<IUserBan>>();

    useEffect(() => {
        loadUserBans();
    }, [props.banned]);

    const loadUserBans = async () => {
        const res = await Api.get('user/bansList?id=' + user.id);
        if (res.bansList) {
            setBansList(res.bansList);
            if (res.bansList[0] && res.bansList[0].banned_until * 1000 >= Date.now()) {
                props.setBanned(true);
            } else {
                props.setBanned(false);
            }
        }
    }

    return (
        <PageBlock title="Dernier bannissement">
            {bansList && bansList.length > 0 ?
                <table className="table table-hover table-dark">
                    <thead>
                        <tr>
                            <th>Raison</th>
                            <th>Fin du ban</th>
                        </tr>
                    </thead>
                    <tbody>
                        {bansList.map((ban, i) => {
                            return (
                                <tr key={i}>
                                    <td>{ban.message ? ban.message : 'Pas de raison'}</td>
                                    <td>{new Date(ban.banned_until * 1000).toLocaleString()}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                :
                <p className="text-center" style={{fontSize:'1.5em'}}>{user.username} n'a jamais été banni</p>
            }
        </PageBlock>
    );
}