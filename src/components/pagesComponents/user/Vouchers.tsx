import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Api from "../../../Api";
import { IVoucherHistory } from "../../../interfaces/Voucher";
import PageBlock from "../../main/pageContent/PageBlock";

export default function Vouchers(props: any) {

    const {user} = props;

    const dispatch = useDispatch();
    const [vouchersList, setVouchersList] = useState<Array<IVoucherHistory>>();

    useEffect(() => {
        loadVouchersList();
    }, []);

    const loadVouchersList = async () => {
        const res = await Api.get('user/vouchersList?id=' + user.id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.vouchersList) {
            setVouchersList(res.vouchersList);
        }
    }

    return (
        <PageBlock title="20 derniers bons d'achats">
            {vouchersList && vouchersList.length > 0 ?
                <table className="table table-hover table-dark">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Date</th>
                            <th>Crédits</th>
                            <th>Mobis</th>
                        </tr>
                    </thead>
                    <tbody>
                        {vouchersList.map((voucher) => {
                            return (
                                <tr key={voucher.voucher_code}>
                                    <td>{voucher.voucher_code}</td>
                                    <td>{new Date(voucher.used_at).toLocaleString()}</td>
                                    <td>{voucher.credits_redeemed || 0}</td>
                                    <td>{voucher.items_redeemed || 0}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                :
                <p className="text-center" style={{ fontSize: '1.5em' }}>{user.username} n'a jamais utilisé de bon d'achat</p>
            }
        </PageBlock>
    );
}