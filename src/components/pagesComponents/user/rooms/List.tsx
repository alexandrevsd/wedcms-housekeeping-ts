import { useEffect } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import Api from "../../../../Api";
import { enterHotelRoom } from "../../../../Habbo";
import { IRoom } from "../../../../interfaces/Room";
import ActionButton from "../../../main/buttons/ActionButton";
import PageBlock from "../../../main/pageContent/PageBlock";

export default function List(props: any) {

    const { user, page, setPage, setMaxPage, setNext, setPrevious } = props;
    const dispatch = useDispatch();

    const [loading, setLoading] = useState<boolean>(true);
    const [rooms, setRooms] = useState<Array<IRoom>>();

    useEffect(() => {
        loadRooms();
    }, [page]);

    const loadRooms = async () => {
        const res = await Api.get('user/rooms?id=' + user.id + '&page=' + page);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            setPage(res.page);
            setMaxPage(res.maxPage);
            setNext(res.next);
            setPrevious(res.previous);
            setRooms(res.rooms);
        }
        setLoading(false);
    }

    const handleButtonTeleportToRoom = (roomId: number) => {
        enterHotelRoom(roomId);
    }

    const roomAccessTypes = new Map();
    roomAccessTypes.set(0, 'Ouvert');
    roomAccessTypes.set(1, 'Fermé');
    roomAccessTypes.set(2, 'Mot de passe');

    return loading ?
        <p className="pl-4">Chargement en cours...</p>
        :
        rooms && rooms.length > 0 ?
            <table className="table table-hover table-dark text-center">
                <thead>
                    <tr>
                        <th></th>
                        <th>Nom</th>
                        <th>Connectés</th>
                        <th>Accès</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {rooms.map((room: IRoom, i) => {
                        return <tr>
                            <td className="pl-4 align-middle">
                                <img src={"/images/rooms-models/" + room.model + ".png"} />
                            </td>
                            <td className="align-middle">{room.name}</td>
                            <td className="align-middle">{room.visitors_now}</td>
                            <td className="align-middle">{roomAccessTypes.get(room.accesstype)}</td>
                            <td className="align-middle">
                                <ActionButton color="primary" icon="cog" title="Paramètres de l'appartement" to={"/housekeeping/room/" + room.id + "?userId=" + user.id} />
                                <ActionButton color="primary" icon="box-open" title="Objets dans l'appartement" to={"/housekeeping/room/" + room.id + "/items" + "?userId=" + user.id} />
                                <ActionButton color="primary" icon="comment" title="Logs du chat" to={"/housekeeping/room/" + room.id + "/logs" + "?userId=" + user.id} />
                                <ActionButton color="primary" icon="sign-in-alt" title="Se téléporter dedans" onClick={() => handleButtonTeleportToRoom(room.id)}/>
                            </td>
                        </tr>
                    })}
                </tbody>
            </table>
            :
            <PageBlock className="px-5">
                <p className="text-center" style={{ fontSize: '1.5em' }}>{user.username} n'as pas d'appartements.</p>
            </PageBlock>;
}