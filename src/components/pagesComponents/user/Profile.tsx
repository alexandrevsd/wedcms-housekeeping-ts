import PageBlock from "../../main/pageContent/PageBlock";
import Informations from "./profile/Informations";
import Form from "./profile/Form";
import HabboClub from "./profile/HabboClub";

export default function Profile(props: any) {

    const {user, setUser, ip, banned} = props;

    const hcMember = (
        user
        && user.club_expiration !== 0
        && user.club_expiration < Date.now()
    );

    return (
        <PageBlock>
            <Informations hcMember={hcMember} banned={banned} ip={ip} user={user} />
            <Form user={user} setUser={setUser} />
            <HabboClub hcMember={hcMember} user={user} setUser={setUser} />
        </PageBlock>
    );
}