import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import Api from "../../../../Api";
import useModal from "../../../../hooks/useModal";
import { IItem, IItemDefinition, IItemImage } from "../../../../interfaces/Item";
import ActionButton from "../../../main/buttons/ActionButton";
import Button from "../../../main/buttons/Button";
import PageButton from "../../../main/buttons/PageButton";
import Modal from "../../../main/Modal";
import PageBlock from "../../../main/pageContent/PageBlock";

export default function List(props: any) {

    const { user, page, setPage, setPrevious, setNext, setMaxPage } = props;

    const dispatch = useDispatch();

    const { isShowing, toggle: toggleConfirm } = useModal();
    const [loading, setLoading] = useState<boolean>(true);
    const [items, setItems] = useState<Array<IItem>>();
    const [itemDefinitions, setItemDefinitions] = useState<Array<IItemDefinition>>();
    const [itemImages, setItemImages] = useState<Array<IItemImage>>();
    const [modalContent, setModalContent] = useState<string>('');

    useEffect(() => {
        loadHand();
    }, [page]);

    const loadHand = async () => {
        const res = await Api.get('user/hand?id=' + user.id + '&page=' + page);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            setItemImages(res.itemImages);
            setItemDefinitions(res.itemDefinitions);
            setItems(res.items);
            setPage(parseInt(res.page));
            setPrevious(res.previous);
            setNext(res.next);
            setMaxPage(res.maxPage);
        }
        setLoading(false);
    }

    const handleButtonDeleteFurni = async (id: number) => {
        const res = await Api.delete('item?id=' + id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            toast.success("Le mobis a bien été supprimé.")
            loadHand();
        }
    }

    const handleButtonDeleteHandConfirm = (e: any) => {
        e.preventDefault();
        if (user) {
            setModalContent("Voulez-vous vraiment vider la main de " + user.username + " ?")
            toggleConfirm();
        }
    }

    const handleButtonDeleteHand = async () => {
        toggleConfirm();
        const res = await Api.delete('user/hand?id=' + user.id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            toast.success("La main de " + user.username + " a bien été vidée.")
            loadHand();
        }
    }

    return (
        <PageBlock>
            {items && itemDefinitions && itemImages && items.length > 0 ?
                <>
                    <Button onClick={handleButtonDeleteHandConfirm} color="danger">Vider la main</Button>
                    <Modal
                        title="Attention !"
                        content={modalContent}
                        isShowing={isShowing}
                        onClick={handleButtonDeleteHand}
                        yesButton="Oui"
                        noButton="Annuler"
                        hide={toggleConfirm}
                    />
                    <table className="table table-hover table-dark mt-3">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Nom</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {items?.map((item, i) => {
                                return <tr key={i}>
                                    <td>{itemImages[i] && <img src={'/images/little-furni/' + itemImages[i].little_image + '.gif'} />}</td>
                                    <td>{itemDefinitions[i] && itemDefinitions[i].name}</td>
                                    <td>
                                        <ActionButton onClick={() => handleButtonDeleteFurni(item.id)} color="danger" icon="trash" title="Supprimer le mobi" />
                                    </td>
                                </tr>;
                            })}
                        </tbody>
                    </table>
                </>
                :
                loading ?
                    <p>Chargement...</p>
                    :
                    <p className="text-center" style={{ fontSize: '1.5em' }}>{user.username} n'a pas d'objets dans sa main</p>
            }
        </PageBlock>
    );
}