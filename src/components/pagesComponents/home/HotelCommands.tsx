import PageHalfBlock from "../../main/pageContent/PageHalfBlock";

export default function HotelCommands() {
    return (
        <PageHalfBlock title={"Commandes dans le jeu"}>
            <div className="row">
                <div className="col-lg-6">
                    <p><b>:givebadge</b> <i>{"{NOM_HABBO}"} {"{CODE_BADGE}"}</i><br />Donnes un badge à un joueur</p>
                    <p><b>:givecredits</b> <i>{"{NOM_HABBO}"} {"{MONTANT}"}</i><br />Donnes des crédits à un joueur</p>
                    <p><b>:reload</b> <i>{"{COMPOSANT}"}</i><br />Recharges un composant spécifique (catalogue, shop,
                        items, models, texts ou settings)</p>
                    <p><b>:shutdown</b><br />Éteints le serveur</p>
                    <p><b>:setprice</b> <i>{"{SALE_CODE}"} {"{PRIX}"}</i><br />Configure le prix d'un item dans le catalogue
                    </p>
                    <p><b>:setconfig</b> <i>{"{PARAMETRE}"} {"{VALEUR}"}</i><br />Configure la valeur d'un paramètre du
                        serveur</p>
                    <p><b>:hotelalert</b> <i>{"{MESSAGE}"}</i><br />Envoies un message d'alerte à tous les joueurs en
                        ligne</p>
                    <p><b>:ufos</b><br />Fais apparaître des OVNIs dans un appart</p>
                </div>
                <div className="col-lg-6">
                    <p><b>:talk</b> <i>{"{MESSAGE}"}</i><br />Parle à tous les joueurs</p>
                    <p><b>:bus</b> <i>open</i><br />Ouvres la porte de l'InfoBus</p>
                    <p><b>:bus</b> <i>close</i><br />Fermes la porte de l'InfoBus</p>
                    <p><b>:bus</b> <i>question {"{QUESTION}"}</i><br />Changes la question de l'InfoBus</p>
                    <p><b>:bus</b> <i>option add {"{REPONSE}"}</i><br />Ajoutes une réponse possible à l'InfoBus</p>
                    <p><b>:bus</b> <i>option remove {"{NUMERO_REPONSE}"}</i><br />Supprimes une réponse de l'InfoBus</p>
                    <p><b>:bus</b> <i>status</i><br />Affiches l'état de l'InfoBus</p>
                    <p><b>:bus</b> <i>reset</i><br />Remets à zéro l'InfoBus</p>
                </div>
            </div>
        </PageHalfBlock>
    );
}