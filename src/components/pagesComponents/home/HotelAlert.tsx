import { useState } from "react";
import { useDispatch } from "react-redux";
import Api from "../../../Api";
import { toast } from 'react-toastify';

import Input from "../../main/form/Input";
import SubmitButton from "../../main/form/SubmitButton";
import PageHalfBlock from "../../main/pageContent/PageHalfBlock";

export default function HotelAlert() {

    const dispatch = useDispatch();

    const [inputMessage, setInputMessage] = useState<string>('');

    const handleInputMessage = (e: any) => setInputMessage(e.target.value);

    const handleSubmitAlert = async (e: any) => {
        e.preventDefault();
        e.target.disabled = true;
        if (inputMessage !== '') {
            const res = await Api.post('rcon/sendHotelAlert', { message: inputMessage });
            const logged = Api.checkLogged(res, dispatch);
            if (logged) {
                if(res.response === 200) {
                    setInputMessage('');
                    toast.success("Le message a bien été envoyé !");
                } else {
                    toast.error("Une erreur s'est produite durant l'envoi.")
                }
            }
        } else {
            toast.error("Tu dois d'abord entrer un message.")
        }
        e.target.disabled = false;
    }

    return (
        <PageHalfBlock title={"Alerte hôtel"}>
            <form>
                <Input placeholder="Message de l'alerte" value={inputMessage} onChange={handleInputMessage} type="text" id="message" />
                <SubmitButton onClick={handleSubmitAlert} color="primary">Envoyer</SubmitButton>
            </form>
            <p className="text-gray-800" style={{ fontSize: '0.9em' }}>
                &lt;br&gt; peut être utilisé pour sauter une ligne
            </p>
        </PageHalfBlock>
    );
}