import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import Api from "../../../Api";

import Counter from "./counters/Counter";

export default function CounterBlock() {

    const dispatch = useDispatch();

    const [onlineUsers, setOnlineUsers] = useState<number>(0);
    const [registeredUsers, setRegisteredUsers] = useState<number>(0);
    const [rooms, setRooms] = useState<number>(0);
    const [furnis, setFurnis] = useState<number>(0);

    useEffect(() => {
        loadCounters();
    }, []);

    const loadCounters = async () => {
        const res = await Api.get('misc/counters');
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.response === 200) {
            setOnlineUsers(res.onlineUsers);
            setRegisteredUsers(res.registeredUsers);
            setRooms(res.rooms);
            setFurnis(res.furnis);
        }
    }

    return (
        <div className="row">
            <Counter color="success" icon="globe" text="Connectés" value={onlineUsers} />
            <Counter color="warning" icon="user-edit" text="Inscrits" value={registeredUsers} />
            <Counter color="light" icon="building" text="Apparts" value={rooms} />
            <Counter color="info" icon="box-open" text="Mobis" value={furnis} />
        </div>
    );
}