import PageHalfBlock from "../../main/pageContent/PageHalfBlock";

export default function WedCmsInfos() {
    return (
        <PageHalfBlock title={"WedCMS"}>
            <p>Version 1.0</p>
            <p>Merci à :</p>
            <ul className="credits-list">
                <li><b><a href="https://github.com/Quackster" target="_blank" rel="noreferrer">Quackster</a></b>:
                    Pour le serveur Kepler et les archives de développement Habbo</li>
                <li><b><a href="https://alex-dev.org/archive/" target="_blank" rel="noreferrer">Archives de
                    Quackster</a></b>: Pour les DCRs, les mobis ajoutés et d'autres ressources</li>
                <li><b><a href="https://github.com/Quackster/Kepler/" target="_blank"
                    rel="noreferrer">Kepler</a></b>: Pour être le meilleur serveur Habbo old school</li>
                <li><b><a href="http://forum.ragezone.com/f353/website-habbsite-cms-released-249645/"
                    target="_blank" rel="noreferrer">Habbsite CMS</a></b>: Pour l'inspiration de la home
                    page du site</li>
                <li><b><a href="https://web.archive.org/web/20070105050103/http://www.habbo.fr/" target="_blank"
                    rel="noreferrer">Habbo France 2007</a></b>: Pour avoir été scrapé jusqu'à l'os</li>
                <li><b><a href="https://startbootstrap.com/theme/sb-admin-2" target="_blank" rel="noreferrer">SB
                    Admin 2</a></b>: Pour être le thème utilisé sur le Housekeeping</li>
                <li><b><a href="https://discord.com/users/708445578744561764" target="_blank"
                    rel="noreferrer">Valen001</a></b>: Pour avoir aidé à compiler la version 1.33 de Kepler
                    et apporté du soutien</li>
                <li><b><a href="https://devbest.com/" target="_blank" rel="noreferrer">DevBest</a> &amp; <a
                    href="https://forum.ragezone.com/" target="_blank" rel="noreferrer">Ragezone</a></b>:
                    Pour les ressources de développement et la communauté</li>
            </ul>
        </PageHalfBlock>
    );
}