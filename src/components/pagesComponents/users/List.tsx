import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Api from "../../../Api";
import useModal from "../../../hooks/useModal";
import { IUser } from "../../../interfaces/User";
import Modal from "../../main/Modal";
import Item from "./list/Item";

export default function UsersList(props: any) {

    const { page, setPage, setNext, setPrevious, setMaxPage } = props;

    const dispatch = useDispatch();

    const [users, setUsers] = useState<Array<IUser>>([]);

    useEffect(() => {
        loadUsers();
    }, [page]);

    const loadUsers = async () => {
        const res = await Api.get('users?page=' + page);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.users) {
            setUsers(res.users);
            setPage(res.page);
            setNext(res.next);
            setPrevious(res.previous);
            setMaxPage(res.maxPage);
        }
    }

    return (
        <table className="table table-hover table-dark">
            <thead>
                <tr>
                    <th></th>
                    <th>Nom</th>
                    <th>Rang</th>
                    <th>Dernière connexion</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {users.map((user, i) => {
                    return <Item
                        user={user}
                        index={i}
                        key={i}
                    />
                })}
            </tbody>
        </table>
    );
}