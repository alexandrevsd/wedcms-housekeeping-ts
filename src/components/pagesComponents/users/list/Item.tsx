import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { IUserItem } from "../../../../interfaces/User";
import { userSelectors } from "../../../../store/userSelectors";
import ActionButton from "../../../main/buttons/ActionButton";

export default function Item({
    user,
    index
}: IUserItem) {    
    const userState = useSelector(userSelectors);
    const locked = (
        (userState.user.id === user.id)
        || (userState.user.rank <= user.rank)
    );
    return (
        <tr>
            <td className="align-middle pl-4">
                <div className="user-profile-image">
                    <Link to={"/housekeeping/user/" + user.id} title="Profil de l'utilisateur">
                        <img src={"/habbo-imager/?figure=" + user.figure + "&reverse=true"} />
                    </Link>
                </div>
            </td>
            <td className="align-middle">{user.username}</td>
            <td className="align-middle">{userState.rankNames.get(user.rank)}</td>
            <td className="align-middle">{user.last_online === 0 ? 'Jamais' : new Date(user.last_online * 1000).toLocaleString()}</td>
            <td className="align-middle pr-4 text-right">
                <ActionButton color="primary" icon="user" title="Profil de l'utilisateur" to={"/housekeeping/user/" + user.id} />
                <ActionButton color="primary" icon="building" title="Voir ses appartements" to={"/housekeeping/user/" + user.id + "/rooms"} />
                <ActionButton color="primary" icon="hand-holding" title="Voir sa main" to={"/housekeeping/user/" + user.id + "/hand"} />
            </td>
        </tr>
    );
}