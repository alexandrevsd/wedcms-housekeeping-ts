import { ICataloguePage } from "../../../interfaces/Page";
import { Droppable } from 'react-beautiful-dnd';

import Item from "./list/Item";
import Table from "../../main/table/Table";
import TableHead from "../../main/table/TableHead";

export default function List(props: any) {

    const pages: Array<ICataloguePage> = props.pages;

    return (
        <Table>
            <TableHead>
                <th></th>
                <th>Image</th>
                <th>Nom</th>
                <th>Cachée</th>
                <th>Rang min.</th>
                <th>HC requis</th>
                <th></th>
            </TableHead>
            <Droppable droppableId="droppable">
                {(provided) => (
                    <tbody
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                    >
                        {pages.map((page: ICataloguePage, i: number) => (
                            <Item page={page} key={'catalog-page-' + i} index={i} />
                        ))}
                        {provided.placeholder}
                    </tbody>
                )}
            </Droppable>
        </Table>
    );
}