import { useSelector } from "react-redux";
import { Draggable } from "react-beautiful-dnd";

import { ICataloguePage } from "../../../../interfaces/Page";
import { userSelectors } from "../../../../store/userSelectors";

import ActionButton from "../../../main/buttons/ActionButton";
import TdMiddle from "../../../main/table/TdMiddle";

export default function Item(props: any) {

    const userState = useSelector(userSelectors);

    const cataloguePage: ICataloguePage = props.page;

    return (
        <Draggable key={cataloguePage.id} draggableId={cataloguePage.id.toString()} index={props.index}>
            {(provided) => (
                <tr
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    className={!cataloguePage.index_visible ? "wd-table-danger" : ""}
                >
                    <TdMiddle className="pl-4" {...provided.dragHandleProps}>
                        <i className="fas fa-bars" />
                    </TdMiddle>
                    <TdMiddle>
                        <img src={'/images/catalog-headers/' + cataloguePage.image_headline + '.gif'} />
                    </TdMiddle>
                    <TdMiddle>
                        {cataloguePage.name}
                    </TdMiddle>
                    <TdMiddle>
                        {cataloguePage.index_visible ? '-' : 'Cachée'}
                    </TdMiddle>
                    <TdMiddle>
                        {userState.rankNames.get(cataloguePage.min_role)}
                    </TdMiddle>
                    <TdMiddle>
                        {cataloguePage.is_club_only ? <img src={'/v14/c_images/badges/wedcms/HC1.gif'} /> : '-'}
                    </TdMiddle>
                    <TdMiddle className="pr-4">
                        <ActionButton to={'/housekeeping/catalogue/page/' + cataloguePage.id} color="primary" icon="pen" title="Modifier les informations" />
                        {cataloguePage.layout === 'ctlg_layout2' &&
                            <ActionButton to={'/housekeeping/catalogue/page/' + cataloguePage.id + '/items'} color="primary" icon="box-open" title="Modifier les objets" />
                        }
                    </TdMiddle>
                </tr>
            )}
        </Draggable>
    );
}