import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router";

import Api from "../../../../Api";
import { ICatalogueItem, ICataloguePackage } from "../../../../interfaces/CatalogueItem";
import { IItemDefinition } from "../../../../interfaces/Item";
import { getItemNamesAndDescs } from "../../../../utils";

import Checkbox from "../../../main/form/Checkbox";
import Input from "../../../main/form/Input";
import Select from "../../../main/form/Select";
import SubmitButton from "../../../main/form/SubmitButton";

export default function Form(props: any) {

    const { pageId, itemId } = useParams();
    const item: ICatalogueItem = props.item;
    const cataloguePackage: ICataloguePackage = props.cataloguePackage;
    const dispatch = useDispatch();

    const [loading, setLoading] = useState<boolean>(true);
    const [items, setItems] = useState<Array<IItemDefinition>>();
    const [itemNames, setItemNames] = useState<Map<string, string>>();
    const [posterNames, setPosterNames] = useState<Map<string, string>>();
    const [inputName, setInputName] = useState<string>(item.is_package ? item.package_name : item.name);
    const [inputDescription, setInputDescription] = useState<string>(item.is_package ? item.package_description : item.description);
    const [inputAmount, setInputAmount] = useState<number>(item.is_package ? cataloguePackage.amount : 1);
    const [inputPrice, setInputPrice] = useState<number>(item.price);
    const [inputIsHidden, setInputIsHidden] = useState<boolean>(item.is_hidden);
    const [inputDefinitionId, setInputDefinitionId] = useState<number>(item.is_package ? cataloguePackage.definition_id : item.definition_id);
    const [inputSaleCode, setInputSaleCode] = useState<string>(item.sale_code);

    const handleInputName = (e: any) => setInputName(e.target.value);
    const handleInputDescription = (e: any) => setInputDescription(e.target.value);
    const handleInputAmount = (e: any) => setInputAmount(e.target.value);
    const handleInputPrice = (e: any) => setInputPrice(e.target.value);
    const handleInputIsHidden = (e: any) => setInputIsHidden(e.target.checked);
    const handleInputDefinitionId = (e: any) => setInputDefinitionId(e.target.value);
    const handleInputSaleCode = (e: any) => setInputSaleCode(e.target.value);

    useEffect(() => {
        loadItemsAndTxts();
    }, [])

    const loadItemsAndTxts = async () => {
        const res = await Api.get('misc/texts');
        if (res) {
            const { itemNames: newItemNames, posterNames: newPosterNames } = getItemNamesAndDescs(res);
            setItemNames(newItemNames);
            setPosterNames(newPosterNames);
            const res2 = await Api.get('misc/itemDefinitions');
            const logged = Api.checkLogged(res, dispatch);
            if (logged && res2.itemDefinitions) {
                setItems(res2.itemDefinitions);
                if(itemId === 'new') setInputDefinitionId(res2.itemDefinitions[0].id);
            }
        }
        setLoading(false);
    }

    const handleSubmitPageItem = async (e: any) => {
        e.preventDefault();
        const res = await Api.post('catalogue/page/item?id=' + pageId, {
            item: {
                id: item.id,
                page_id: pageId,
                name: inputName,
                sale_code: inputSaleCode,
                description: inputDescription,
                price: inputPrice,
                is_hidden: inputIsHidden,
                definition_id: inputDefinitionId,
                is_package: inputAmount > 1,
                amount: inputAmount
            }
        })
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            alert('ok')
        }
    }

    return (
        loading ?
            <p>Chargement en cours...</p>
            :
            <form>
                <Input label="Nom de l'objet" onChange={handleInputName} value={inputName} type="text" id="name" />
                {itemId === 'new' && <Input label="Nom de code de l'objet" onChange={handleInputSaleCode} value={inputSaleCode} type="text" id="sale_code" />}
                <Input label="Description de l'objet" onChange={handleInputDescription} value={inputDescription} type="text" id="description" />
                <Input label="Quantité" onChange={handleInputAmount} value={inputAmount} type="number" id="package_amount" />
                <Input label="Prix en crédits" onChange={handleInputPrice} value={inputPrice} type="number" id="price" />
                <Checkbox label="Cacher l'objet" onChange={handleInputIsHidden} checked={inputIsHidden} id="is_hidden" />
                <Select onChange={handleInputDefinitionId} value={inputDefinitionId} label="Objet lié">
                    {items?.map((item: IItemDefinition, i: number) => {
                        if (item.sprite === 'poster') {
                            return <option key={'poster-' + i} value={item.id}>{posterNames?.get(item.sprite_id.toString())} - {item.sprite}</option>
                        } else {
                            return <option key={'item-' + i} value={item.id}>{itemNames?.get(item.sprite)} - {item.sprite}</option>
                        }
                    })}
                </Select>
                <SubmitButton onClick={handleSubmitPageItem} color="primary">Sauvegarder</SubmitButton>
            </form>
    );
}