import { useEffect, useState } from "react";
import CtlgCamera1 from "./labelExtraTForm/CtlgCamera1";
import CtlgClub1 from "./labelExtraTForm/CtlgClub1";
import CtlgClub2 from "./labelExtraTForm/CtlgClub2";
import CtlgFrontpage2 from "./labelExtraTForm/CtlgFrontpage2";
import CtlgPets from "./labelExtraTForm/CtlgPets";
import CtlgPlasto from "./labelExtraTForm/CtlgPlasto";
import CtlgPresents from "./labelExtraTForm/CtlgPresents";
import CtlgSpaces from "./labelExtraTForm/CtlgSpaces";
import CtlgTrophies from "./labelExtraTForm/CtlgTrophies";

export default function LabelExtraTForm(props: any) {

    const page = props.page;
    const onChange = props.onChange;

    const [formData, setFormData] = useState<any>(page.label_extra_t);

    useEffect(() => {
        onChange(formData);
    }, [formData])

    const getForm = () => {
        let Form;
        switch (page.layout) {
            case 'ctlg_frontpage2':
                Form = <CtlgFrontpage2 formData={formData} onFormData={onFormData} />
                break;
            case 'ctlg_spaces':
                Form = <CtlgSpaces formData={formData} onFormData={onFormData} />
                break;
            case 'ctlg_club2':
                Form = <CtlgClub2 formData={formData} onFormData={onFormData} />
                break;
            case 'ctlg_club1':
                Form = <CtlgClub1 formData={formData} onFormData={onFormData} />
                break;
            case 'ctlg_camera1':
                Form = <CtlgCamera1 formData={formData} onFormData={onFormData} />
                break;
            case 'ctlg_plasto':
                Form = <CtlgPlasto formData={formData} onFormData={onFormData} />
                break;
            case 'ctlg_pets':
                Form = <CtlgPets formData={formData} onFormData={onFormData} />
                break;
            case 'ctlg_trophies':
                Form = <CtlgTrophies formData={formData} onFormData={onFormData} />
                break;
            case 'ctlg_presents':
                Form = <CtlgPresents formData={formData} onFormData={onFormData} />
                break;
        }
        return Form;
    }

    const onFormData = (data: string) => {
        setFormData(data);
    }

    return (
        <>
            {getForm()}
        </>
    );
}