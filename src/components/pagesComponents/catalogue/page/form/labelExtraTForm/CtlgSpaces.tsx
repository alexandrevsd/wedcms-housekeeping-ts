import { useEffect, useState } from "react";
import Input from "../../../../../main/form/Input";

export default function CtlgSpaces(props: any) {

    const formData = props.formData;
    const onFormData = props.onFormData;

    const [input1, setInput1] = useState<string>('');
    const [input2, setInput2] = useState<string>('');
    const [input3, setInput3] = useState<string>('');
    const [input4, setInput4] = useState<string>('');
    const [input5, setInput5] = useState<string>('');
    const [input6, setInput6] = useState<string>('');
    const [input7, setInput7] = useState<string>('');

    useEffect(() => {
        const dataSplitted = formData.split('\n');
        dataSplitted.forEach((data: string) => {
            const dataSplitted2 = data.split(':');
            if (dataSplitted2[0] === '1') setInput1(dataSplitted2[1]);
            if (dataSplitted2[0] === '2') setInput2(dataSplitted2[1]);
            if (dataSplitted2[0] === '3') setInput3(dataSplitted2[1]);
            if (dataSplitted2[0] === '4') setInput4(dataSplitted2[1]);
            if (dataSplitted2[0] === '5') setInput5(dataSplitted2[1]);
            if (dataSplitted2[0] === '6') setInput6(dataSplitted2[1]);
            if (dataSplitted2[0] === '7') setInput7(dataSplitted2[1]);
        })
    }, []);

    useEffect(() => {
        transformData();
    }, [input1, input2, input3, input4, input5, input6, input7]);

    const handleInput1 = (e: any) => setInput1(e.target.value);
    const handleInput2 = (e: any) => setInput2(e.target.value);
    const handleInput3 = (e: any) => setInput3(e.target.value);
    const handleInput4 = (e: any) => setInput4(e.target.value);
    const handleInput5 = (e: any) => setInput5(e.target.value);
    const handleInput6 = (e: any) => setInput6(e.target.value);
    const handleInput7 = (e: any) => setInput7(e.target.value);

    const transformData = () => {
        const data = (
            '1:' + input1 + '\r\n'
            + '2:' + input2 + '\r\n'
            + '3:' + input3 + '\r\n'
            + '4:' + input4 + '\r\n'
            + '5:' + input5 + '\r\n'
            + '6:' + input6 + '\r\n'
            + '7:' + input7
            );
        onFormData(data);
    }

    return (
        <>
            <Input onChange={handleInput1} value={input1} label={"Texte titre mur"} id="input1" />
            <Input onChange={handleInput2} value={input2} label={"Texte catégorie sol"} id="input2" />
            <Input onChange={handleInput3} value={input3} label={"Texte séléction motif mur"} id="input3" />
            <Input onChange={handleInput4} value={input4} label={"Texte séléction couleur mur"} id="input4" />
            <Input onChange={handleInput5} value={input5} label={"Texte séléction motif sol"} id="input5" />
            <Input onChange={handleInput6} value={input6} label={"Texte séléction couleur sol"} id="input6" />
            <Input onChange={handleInput7} value={input7} label={"Texte catégorie aperçu"} id="input7" />
        </>
    );
}