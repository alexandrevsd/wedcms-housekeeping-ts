import { useEffect, useState } from "react";
import Input from "../../../../../main/form/Input";

export default function CtlgPets(props: any) {

    const formData = props.formData;
    const onFormData = props.onFormData;

    const [input1, setInput1] = useState<string>('');

    useEffect(() => {
        const dataSplitted = formData.split('\n');
        dataSplitted.forEach((data: string) => {
            const dataSplitted2 = data.split(':');
            if (dataSplitted2[0] === '1') setInput1(dataSplitted2[1]);
        })
    }, []);

    useEffect(() => {
        transformData();
    }, [input1]);

    const handleInput1 = (e: any) => setInput1(e.target.value);

    const transformData = () => {
        const data = (
            '1:' + input1
            );
        onFormData(data);
    }

    return (
        <>
            <Input onChange={handleInput1} value={input1} label={"Texte surnom"} id="input1" />
        </>
    );
}