import { useEffect, useState } from "react";
import Input from "../../../../../main/form/Input";

export default function CtlgPlasto(props: any) {

    const formData = props.formData;
    const onFormData = props.onFormData;

    const [input1, setInput1] = useState<string>('');
    const [input2, setInput2] = useState<string>('');
    const [input3, setInput3] = useState<string>('');

    useEffect(() => {
        const dataSplitted = formData.split('\n');
        dataSplitted.forEach((data: string) => {
            const dataSplitted2 = data.split(':');
            if (dataSplitted2[0] === '1') setInput1(dataSplitted2[1]);
            if (dataSplitted2[0] === '2') setInput2(dataSplitted2[1]);
            if (dataSplitted2[0] === '3') setInput3(dataSplitted2[1]);
        })
    }, []);

    useEffect(() => {
        transformData();
    }, [input1, input2, input3]);

    const handleInput1 = (e: any) => setInput1(e.target.value);
    const handleInput2 = (e: any) => setInput2(e.target.value);
    const handleInput3 = (e: any) => setInput3(e.target.value);

    const transformData = () => {
        const data = (
            '1:' + input1 + '\r\n'
            + '2:' + input2 + '\r\n'
            + '3:' + input3
            );
        onFormData(data);
    }

    return (
        <>
            <Input onChange={handleInput1} value={input1} label={"Texte choix article"} id="input1" />
            <Input onChange={handleInput2} value={input2} label={"Texte choix couleur"} id="input2" />
            <Input onChange={handleInput3} value={input3} label={"Texte aperçu"} id="input3" />
        </>
    );
}