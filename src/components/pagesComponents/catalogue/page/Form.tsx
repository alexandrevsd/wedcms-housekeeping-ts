
// image_headline (banniere)
// 1 image_teaser
// 1 body
// 1 label pick
//1 label extra s

import ScrollableBox, { useDefaultLipClassNames } from 'react-scrollable-box';
import '../../../../css/imagepicker.css'
import { useDispatch, useSelector } from "react-redux";
import { userSelectors } from "../../../../store/userSelectors";
import Checkbox from "../../../main/form/Checkbox";
import Input from "../../../main/form/Input";
import Select from "../../../main/form/Select";
import ImagePicker, { IImagePickerImage } from '../../../main/form/ImagePicker';
import { useEffect, useRef, useState } from 'react';
import Api from '../../../../Api';
import ImageSinglePicker from '../../../main/form/ImageSinglePicker';
import { ICataloguePage } from '../../../../interfaces/Page';
import SubmitButton from '../../../main/form/SubmitButton';
import Textarea from '../../../main/form/Textarea';
import LabelExtraTForm from './form/LabelExtraTForm';
import Modal from '../../../main/Modal';
import useModal from '../../../../hooks/useModal';
import { toast } from 'react-toastify';
import { Navigate } from 'react-router';

export default function Form(props: any) {


    const { setPageName } = props;
    const page: ICataloguePage = props.page;

    let labelInfoBubble = '';
    let themeInfoBubble = 1;
    if (page.label_extra_s) {
        if (page.label_extra_s.startsWith('1:')) {
            labelInfoBubble = page.label_extra_s.substring(2);
        } else if (page.label_extra_s.startsWith('2:')) {
            labelInfoBubble = page.label_extra_s.substring(2);
            themeInfoBubble = 2;
        } else if (page.label_extra_s.startsWith('3:')) {
            labelInfoBubble = page.label_extra_s.substring(2);
            themeInfoBubble = 3;
        } else {
            labelInfoBubble = page.label_extra_s;
        }
    }

    const pageTeasers = page.image_teasers.split(',');
    let pageTeaser1 = '';
    let pageTeaser2 = '';
    let pageTeaser3 = '';
    pageTeasers.forEach((teaserImage: string, index: number) => {
        if (index === 0) pageTeaser1 = teaserImage;
        if (index === 1) pageTeaser2 = teaserImage;
        if (index === 2) pageTeaser3 = teaserImage;
    })

    const userState = useSelector(userSelectors);
    const lipClassNames = useDefaultLipClassNames();
    const dispatch = useDispatch();


    const { isShowing, toggle: toggleConfirm } = useModal();
    const [submittedNew, setSubmittedNew] = useState<boolean>(false);
    const [headers, setHeaders] = useState<Array<IImagePickerImage>>([]);
    const [teasers, setTeasers] = useState<Array<IImagePickerImage>>([]);
    const [inputName, setInputName] = useState<string>(page.name);
    const [inputMinRole, setInputMinRole] = useState<number>(page.min_role);
    const [inputIndexVisible, setInputIndexVisible] = useState<boolean>(page.index_visible);
    const [inputIsClubOnly, setInputIsClubOnly] = useState<boolean>(page.is_club_only);
    const [inputImageHeadline, setInputImageHeadline] = useState<string>(page.image_headline);
    const [inputBody, setInputBody] = useState<string>(page.body);
    const [inputLabelExtraS, setInputLabelExtraS] = useState<string>(labelInfoBubble);
    const [inputInfoBubbleTheme, setInputInfoBubbleTheme] = useState<number>(themeInfoBubble);
    const [inputLabelExtraT, setInputLabelExtraT] = useState<string>(page.label_extra_t);
    const [inputLabelPick, setInputLabelPick] = useState<string>(page.label_pick);
    const [inputLayout, setInputLayout] = useState<string>(page.layout);
    const [tempInputLayout, setTempInputLayout] = useState<string>(page.layout);
    const [inputTeaser1, setInputTeaser1] = useState<string>(pageTeaser1);
    const [inputTeaser2, setInputTeaser2] = useState<string>(pageTeaser2);
    const [inputTeaser3, setInputTeaser3] = useState<string>(pageTeaser3);
    const [toggleInfoBubble, setToggleInfoBubble] = useState<boolean>(page.label_extra_s !== '' && page.label_extra_s !== null && page.label_extra_s !== undefined);

    useEffect(() => {
        loadHeaders();
        loadTeasers();
    }, []);

    const loadHeaders = async () => {
        const res = await Api.get('catalogue/headers');
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.headers) {
            const resHeaders: Array<IImagePickerImage> = [];
            res.headers.forEach((header: string) => {
                resHeaders.push({
                    src: '/images/catalog-headers/' + header + '.gif',
                    value: header
                })
            })
            setHeaders(resHeaders);
        }
    }

    const loadTeasers = async () => {
        const res = await Api.get('catalogue/teasers');
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.teasers) {
            const resTeasers: Array<IImagePickerImage> = [];
            res.teasers.forEach((teaser: string) => {
                resTeasers.push({
                    src: '/images/catalog-teasers/' + teaser + '.gif',
                    value: teaser
                })
            })
            setTeasers(resTeasers);
        }
    }

    const handleInputName = (e: any) => setInputName(e.target.value);
    const handleInputMinRole = (e: any) => setInputMinRole(e.target.value);
    const handleInputIndexVisible = (e: any) => setInputIndexVisible(e.target.checked);
    const handleInputIsClubOnly = (e: any) => setInputIsClubOnly(e.target.checked);
    const handleImageSelect = (value: string) => setInputImageHeadline(value);
    const handleInputTeaser1 = (value: string) => setInputTeaser1(value);
    const handleInputTeaser2 = (value: string) => setInputTeaser2(value);
    const handleInputTeaser3 = (value: string) => setInputTeaser3(value);
    const handleInputBody = (e: any) => setInputBody(e.target.value);
    const handleToggleInfoBubble = (e: any) => setToggleInfoBubble(e.target.checked);
    const handleInputLabelExtraS = (e: any) => setInputLabelExtraS(e.target.value);
    const handleInputInfoBubbleTheme = (e: any) => setInputInfoBubbleTheme(e.target.value);
    const handleInputLabelExtraT = (data: string) => setInputLabelExtraT(data);
    const handleInputLabelPick = (e: any) => setInputLabelPick(e.target.value);
    const handleInputLayout = () => {
        toggleConfirm();
        setInputLayout(tempInputLayout);
        page.layout = tempInputLayout;
    }

    const handleInputLayoutConfirm = (e: any) => {
        setTempInputLayout(e.target.value);
        toggleConfirm();
    };


    const getInputImageTeasers = () => {
        let imageTeasers = '';
        if (inputTeaser1 !== '') {
            imageTeasers = inputTeaser1;
            if (inputTeaser2 !== '') {
                imageTeasers += (',' + inputTeaser2);
                if (inputTeaser3 !== '') {
                    imageTeasers += (',' + inputTeaser3);
                }
            }
        }
        return imageTeasers;
    }

    const handleSubmitPage = async (e: any) => {
        e.preventDefault();
        const newPage = {
            id: page.id,
            name: inputName,
            index_visible: inputIndexVisible,
            is_club_only: inputIsClubOnly,
            image_headline: inputImageHeadline,
            min_role: inputMinRole,
            body: inputBody,
            toggleInfoBubble: toggleInfoBubble,
            label_extra_s: inputInfoBubbleTheme + ':' + inputLabelExtraS,
            label_extra_t: inputLabelExtraT,
            label_pick: inputLabelPick,
            image_teasers: getInputImageTeasers(),
            layout: inputLayout
        }
        const res = await Api.post('catalogue/page', { page: newPage });
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            setPageName(newPage.name);
            toast.success("Les changements ont bien été pris en compte. Un redémarrage du serveur est nécessaire.");
            if (!page.id) {
                setSubmittedNew(true);
            }
        }
    }


    const options: Array<JSX.Element> = [];
    userState.rankNames.forEach((rank: string, i: number) => {
        options.push(<option value={i} key={i}>{rank}</option>);
    })

    return (
        submittedNew ?
            <Navigate to="/housekeeping/catalogue" />
            :
            <form>
                <Modal
                    title="Attention !"
                    content="Changer le layout d'une page peut supprimer des données de la page. Tu es sûr de vouloir continuer ?"
                    isShowing={isShowing}
                    onClick={handleInputLayout}
                    yesButton="Oui"
                    noButton="Annuler"
                    hide={toggleConfirm}
                />
                <Input onChange={handleInputName} value={inputName} type="text" label="Nom de la page" id="name" />
                <Select onChange={handleInputMinRole} value={inputMinRole} label="Rôle minimum" id="min_role">
                    {options}
                </Select>
                <Checkbox onChange={handleInputIndexVisible} checked={inputIndexVisible} label="Afficher dans le catalogue" id="index_visible" />
                <Checkbox onChange={handleInputIsClubOnly} checked={inputIsClubOnly} label="Uniquement pour les HC" id="is_club_only" />
                <Select onChange={handleInputLayoutConfirm} value={inputLayout} label="Layout de la page" id="min_role">
                    <option value="ctlg_layout2">Page basique</option>
                    <option value="ctlg_soundmachine">Page basique + extraits musique</option>
                    <option value="ctlg_productpage1">Page produit 1</option>
                    <option value="ctlg_productpage3">Page produit 2</option>
                    <option value="ctlg_frontpage2">Page d'accueil</option>
                    <option value="ctlg_club1">Page HC 1</option>
                    <option value="ctlg_club2">Page HC 2</option>
                    <option value="ctlg_plasto">Page plastic</option>
                    <option value="ctlg_recycler">Page ecotron</option>
                    <option value="ctlg_pets">Page animaux</option>
                    <option value="ctlg_trophies">Page trophées</option>
                    <option value="ctlg_spaces">Page revêtements</option>
                    <option value="ctlg_camera1">Page photo 1</option>
                    <option value="ctlg_camera2">Page photo 2</option>
                    <option value="ctlg_presents">Page cadeaux</option>
                </Select>
                <div className='mb-3'>
                    <label>Image header</label>
                    <ScrollableBox
                        {...lipClassNames}
                        style={{ maxHeight: '340px', overflow: 'auto' }}
                    >
                        <ImageSinglePicker
                            images={headers}
                            selected={page.image_headline}
                            onSelect={handleImageSelect}
                        />
                    </ScrollableBox>
                </div>
                {(page.layout !== 'ctlg_productpage1' && page.layout !== 'ctlg_plasto' && page.layout !== 'ctlg_recycler' && page.layout !== 'ctlg_pets' && page.layout !== 'ctlg_trophies' && page.layout !== 'ctlg_spaces') &&
                    <>
                        <div className='mb-3'>
                            <label>Image teaser 1</label>
                            <ScrollableBox
                                {...lipClassNames}
                                style={{ maxHeight: '340px', overflow: 'auto' }}
                            >
                                <ImageSinglePicker
                                    images={teasers}
                                    selected={pageTeaser1}
                                    onSelect={handleInputTeaser1}
                                />
                            </ScrollableBox>
                        </div>
                        {(page.layout !== 'ctlg_layout2' && page.layout !== 'ctlg_soundmachine' && page.layout !== 'ctlg_camera2' && page.layout !== 'ctlg_club1') &&
                            <>
                                <div className='mb-3'>
                                    <label>Image teaser 2</label>
                                    <ScrollableBox
                                        {...lipClassNames}
                                        style={{ maxHeight: '340px', overflow: 'auto' }}
                                    >
                                        <ImageSinglePicker
                                            images={teasers}
                                            selected={pageTeaser2}
                                            onSelect={handleInputTeaser2}
                                        />
                                    </ScrollableBox>
                                </div>
                                {page.layout === 'ctlg_productpage3' &&
                                    <div className='mb-3'>
                                        <label>Image teaser 3</label>
                                        <ScrollableBox
                                            {...lipClassNames}
                                            style={{ maxHeight: '340px', overflow: 'auto' }}
                                        >
                                            <ImageSinglePicker
                                                images={teasers}
                                                selected={pageTeaser3}
                                                onSelect={handleInputTeaser3}
                                            />
                                        </ScrollableBox>
                                    </div>
                                }
                            </>
                        }
                    </>
                }
                {page.layout !== 'ctlg_recycler' && page.layout !== 'ctlg_club1' && page.layout !== 'ctlg_club2' &&
                    <Textarea label="Texte de la page" onChange={handleInputBody} value={inputBody} id="body" />
                }
                {(page.layout === 'ctlg_frontpage2' || page.layout === 'ctlg_plasto' || page.layout === 'ctlg_layout2') && <>
                    <Checkbox onChange={handleToggleInfoBubble} checked={toggleInfoBubble} label="Activer l'infobulle" id="toggle_info_bubble" />
                    {toggleInfoBubble && <>
                        <Input onChange={handleInputLabelExtraS} value={inputLabelExtraS} type="text" label="Texte de l'infobulle" id="label_extra_s" />
                        <Select onChange={handleInputInfoBubbleTheme} value={inputInfoBubbleTheme} label="Thème de l'infobulle" id="min_role">
                            <option value="1">Jaune (style promotion)</option>
                            <option value="2">Blanc (style discussion)</option>
                            <option value="3">Rien (juste l'écriture)</option>
                        </Select>
                    </>}
                </>}
                <LabelExtraTForm page={page} onChange={handleInputLabelExtraT} />
                {(page.layout === 'ctlg_layout2' || page.layout === 'ctlg_productpage3' || page.layout === 'ctlg_soundmachine' || page.layout === 'ctlg_plasto') &&
                    <Input onChange={handleInputLabelPick} value={inputLabelPick} type="text" label="Texte séléctionner un objet" id="label_pick" />
                }
                <SubmitButton onClick={handleSubmitPage} className="mt-3" color={page.id ? 'primary' : 'success'}>{page.id ? 'Sauvegarder' : 'Ajouter'}</SubmitButton>
            </form>
    );
}