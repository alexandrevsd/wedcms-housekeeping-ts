import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import Api from "../../../../Api";
import useModal from "../../../../hooks/useModal";
import { ICatalogueItem, IPackage } from "../../../../interfaces/CatalogueItem";
import { IItem, IItemDefinition, IItemImage } from "../../../../interfaces/Item";
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import ActionButton from "../../../main/buttons/ActionButton";
import Button from "../../../main/buttons/Button";
import PageButton from "../../../main/buttons/PageButton";
import Modal from "../../../main/Modal";
import PageBlock from "../../../main/pageContent/PageBlock";


const reorder = (list: Array<any>, startIndex: number, endIndex: number): Array<any> => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};

export default function List(props: any) {

    const { page } = props;

    const dispatch = useDispatch();

    const { isShowing, toggle: toggleConfirm } = useModal();
    const [loading, setLoading] = useState<boolean>(true);
    const [items, setItems] = useState<Array<ICatalogueItem>>([]);
    const [itemDefinitions, setItemDefinitions] = useState<Map<number, IItemDefinition>>(new Map());
    const [itemImages, setItemImages] = useState<Map<number, IItemImage>>(new Map());
    const [packages, setPackages] = useState<Map<string, IPackage>>(new Map());
    const [modalContent, setModalContent] = useState<string>('');

    useEffect(() => {
        loadPageItems();
    }, []);

    const loadPageItems = async () => {
        const res = await Api.get('catalogue/page/items?id=' + page.id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.items && res.itemDefinitions && res.itemImages) {
            setItems(res.items);
            const itemDefinitionsMap = new Map<number, IItemDefinition>();
            res.itemDefinitions.forEach((itemDefinition: IItemDefinition) => {
                itemDefinition !== null && itemDefinitionsMap.set(itemDefinition.id, itemDefinition);
            })
            setItemDefinitions(itemDefinitionsMap);
            const itemImagesMap = new Map<number, IItemImage>();
            res.itemImages.forEach((itemImage: IItemImage) => {
                itemImage !== null && itemImagesMap.set(itemImage.item_definition_id, itemImage);
            })
            setItemImages(itemImagesMap);
            const packagesMap = new Map<string, IPackage>();
            res.itemPackages.forEach((itemPackage: IPackage) => {
                console.log(itemPackage);
                itemPackage !== null && packagesMap.set(itemPackage.salecode, itemPackage);
            })
            setPackages(packagesMap);

        }
        setLoading(false);
    }

    const handleButtonDeleteFurni = async (id: number) => {
        const res = await Api.delete('catalogue/item?id=' + id);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            toast.success("Le mobis a bien été supprimé.");
            loadPageItems();
        }
    }

    const handleDragEnd = (result: any) => {
        if (!result.destination) return;
        const { source, destination } = result;
        const itemsReorder = reorder(items, source.index, destination.index);
        setItems(itemsReorder);
        handleEditOrder(itemsReorder);
    }

    const handleEditOrder = async (items: Array<ICatalogueItem>) => {
        const res = await Api.post('catalogue/items/order', { items });
        Api.checkLogged(res, dispatch);
        console.log(res);
        
    }

    return (
        <PageBlock>
            {items && itemDefinitions && itemImages && packages && items.length > 0 ?
                <DragDropContext
                    onDragEnd={handleDragEnd}
                >
                    <table className="table table-hover table-dark mt-3">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Nom</th>
                                <th>Prix</th>
                                <th></th>
                            </tr>
                        </thead>
                        <Droppable droppableId="droppable2">
                            {(provided) => (
                                <tbody
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                >
                                    {items?.map((item, index) => {
                                        const itemPackage = packages.get(item.sale_code);
                                        let itemImage: IItemImage;
                                        if (item.is_package && itemPackage) {
                                            const image: IItemImage | undefined = itemImages.get(itemPackage.definition_id);
                                            if (image) itemImage = image;
                                        } else {
                                            const image: IItemImage | undefined = itemImages.get(item.definition_id);
                                            if (image) itemImage = image;
                                        }
                                        return (
                                            <Draggable key={item.id} draggableId={item.id.toString()} index={index}>
                                                {(provided) => (
                                                    <tr
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}

                                                        className={item.is_hidden ? "wd-table-danger" : ""}
                                                    >
                                                        <td className="pl-4 align-middle" {...provided.dragHandleProps}><i className="fas fa-bars" /></td>
                                                        <td>
                                                            {itemImage && <img src={'/images/little-furni/' + itemImage.little_image + '.gif'} />}
                                                            {item.is_package && itemPackage && 'x' + itemPackage?.amount}
                                                        </td>
                                                        <td>{item.name || item.package_name}</td>
                                                        <td>{item.price}</td>
                                                        <td>
                                                            <ActionButton to={'/housekeeping/catalogue/page/' + page.id + '/items/' + item.id} color="primary" icon="pen" title="Modifier l'objet" />
                                                            <ActionButton onClick={() => handleButtonDeleteFurni(item.id)} color="danger" icon="trash" title="Supprimer l'objet" />
                                                        </td>
                                                    </tr>
                                                )}
                                            </Draggable>);
                                    })}
                                    {provided.placeholder}
                                </tbody>
                            )}
                        </Droppable>
                    </table>
                </DragDropContext>
                :
                loading ?
                    <p>Chargement...</p>
                    :
                    <p className="text-center" style={{ fontSize: '1.5em' }}>Il n'y a pas encore d'objets dans cette page.</p>
            }
        </PageBlock>
    );
}