import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate } from 'react-router';
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import Api from "../../../Api";
import useModal from "../../../hooks/useModal";
import { ICataloguePage } from "../../../interfaces/Page";
import PageButton from "../../main/buttons/PageButton";
import Modal from "../../main/Modal";
import PageContent from "../../main/PageContent";
import PageBlock from "../../main/pageContent/PageBlock";
import Form from "../../pagesComponents/catalogue/page/Form";

export default function Page() {

    const dispatch = useDispatch();

    const { pageId } = useParams();

    const { isShowing, toggle: toggleConfirm } = useModal();
    const [modalContent, setModalContent] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(true);
    const [deleted, setDeleted] = useState<boolean>(false);
    const [page, setPage] = useState<ICataloguePage>();
    const [pageName, setPageName] = useState<string>('');

    useEffect(() => {
        loadPage();
    }, []);

    const loadPage = async () => {
        if (pageId !== 'new') {
            const res = await Api.get('catalogue/page?id=' + pageId);
            const logged = Api.checkLogged(res, dispatch);
            if (logged && res.page) {
                setPageName(res.page.name);
                setPage(res.page);
            }
        }
        setLoading(false);
    }

    const handleButtonDeletePageConfirm = (e: any) => {
        e.preventDefault();
        setModalContent("Voulez-vous vraiment supprimer cette page ?")
        toggleConfirm();
    }

    const handleButtonDeletePage = async (e: any) => {
        e.preventDefault();
        if (page) {
            const res = await Api.delete('catalogue/page?id=' + page.id);
            const logged = Api.checkLogged(res, dispatch);
            if (logged && res.response === 200) {
                setDeleted(true);
                toast.success("La page a bien été supprimé.");
            }
        }
    }

    return (

        deleted ?
            <Navigate to="/housekeeping/catalogue" />
            :
            page ?
                <PageContent
                    title={"Page \"" + pageName + "\""}
                    buttons={<>
                        <PageButton onClick={handleButtonDeletePageConfirm} title="Supprimer la page" icon="trash" color="danger" />
                        <PageButton to={'/housekeeping/catalogue'} title="Retour au catalogue" icon="undo" color="secondary" />
                    </>}
                >
                    <Modal
                        title="Attention !"
                        content={modalContent}
                        isShowing={isShowing}
                        onClick={handleButtonDeletePage}
                        yesButton="Oui"
                        noButton="Annuler"
                        hide={toggleConfirm}
                    />
                    <Form page={page} setPageName={setPageName} />
                </PageContent>
                :
                loading ?
                    <PageContent
                        title={'Chargement'}
                    >
                        <PageBlock>{'Chargement en cours...'}</PageBlock>
                    </PageContent>
                    :
                    <PageContent
                        title={"Nouvelle page"}
                        buttons={<>
                            <PageButton to={'/housekeeping/catalogue'} title="Retour au catalogue" icon="undo" color="secondary" />
                        </>}
                    >
                        <Form page={{
                            name: '',
                            min_role: 1,
                            index_visible: 1,
                            is_club_only: 0,
                            image_headline: '',
                            body: '',
                            label_extra_s: '',
                            label_extra_t: '',
                            label_pick: 'Clique sur un article pour plus d\'informations',
                            image_teasers: '',
                            layout: 'ctlg_layout2'
                        }} setPageName={setPageName} />

                    </PageContent>

    );
}