import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useParams } from "react-router-dom";
import Api from "../../../Api";
import PageButton from "../../main/buttons/PageButton";
import PageContent from "../../main/PageContent";
import PageBlock from "../../main/pageContent/PageBlock";
import { ICataloguePage } from "../../../interfaces/Page";
import List from "../../pagesComponents/catalogue/pageItems/List";

export default function PageItems() {

    const dispatch = useDispatch();

    const { pageId } = useParams();
    const [loading, setLoading] = useState<boolean>(true);
    const [page, setPage] = useState<ICataloguePage>();

    useEffect(() => {
        loadPage();
    }, []);

    const loadPage = async () => {
        const res = await Api.get('catalogue/page?id=' + pageId);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.page) {
            setPage(res.page);
        }
        setLoading(false);
    }
    return (
        loading ?
            <PageContent
                title={"Chargement"}
            >
                <PageBlock>Chargement en cours...</PageBlock>
            </PageContent>
            :
            page ?
                <PageContent
                    title={"Objets de la page \"" + page.name + "\""}
                    buttons={<>
                        <PageButton to={'/housekeeping/catalogue/page/' + page.id} title="Modifier la page" icon="pen" color="primary" />
                        <PageButton to={'/housekeeping/catalogue/page/' + page.id + '/items/new'} title="Ajouter un objet" icon="plus" color="success" />
                        <PageButton to={'/housekeeping/catalogue/'} title="Retour au catalogue" icon="undo" color="secondary" />
                    </>}
                >
                    <List page={page} />
                </PageContent>
                :
                <Navigate to={"/housekeeping/users"} />

    );
}