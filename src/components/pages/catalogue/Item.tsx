import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useParams } from "react-router-dom";
import Api from "../../../Api";
import PageButton from "../../main/buttons/PageButton";
import PageContent from "../../main/PageContent";
import PageBlock from "../../main/pageContent/PageBlock";
import { ICatalogueItem, ICataloguePackage } from "../../../interfaces/CatalogueItem";
import Input from "../../main/form/Input";
import Form from "../../pagesComponents/catalogue/item/Form";

export default function Item() {

    const dispatch = useDispatch();

    const { pageId, itemId } = useParams();
    const [loading, setLoading] = useState<boolean>(true);
    const [item, setItem] = useState<ICatalogueItem>();
    const [cataloguePackage, setCataloguePackage] = useState<ICataloguePackage>();

    useEffect(() => {
        loadPage();
    }, []);

    const loadPage = async () => {
        if (itemId !== 'new') {
            const res = await Api.get('catalogue/item?id=' + itemId);
            const logged = Api.checkLogged(res, dispatch);
            if (logged && res.item) {
                setItem(res.item);
                if(res.cataloguePackage) {
                    setCataloguePackage(res.cataloguePackage);
                    console.log(res.cataloguePackage);
                    
                }
            }
        }
        setLoading(false);
    }

    return (
        loading ?
            <PageContent
                title={"Chargement"}
            >
                <PageBlock>Chargement en cours...</PageBlock>
            </PageContent>
            :
            item ?
                <PageContent
                    title={"Modifier l'objet \"" + (item.is_package ? item.package_name : item.name) + "\""}
                    buttons={<>
                        <PageButton to={'/housekeeping/catalogue/page/' + item.id + '/items/new'} title="Supprimer l'objet" icon="trash" color="danger" />
                        <PageButton to={'/housekeeping/catalogue/page/' + pageId + '/items'} title="Retour à la liste" icon="undo" color="secondary" />
                    </>}
                >
                    <PageBlock>
                        <Form item={item} cataloguePackage={cataloguePackage} />
                    </PageBlock>
                </PageContent>
                :

                <PageContent
                    title={"Nouvel objet"}
                    buttons={<>
                        <PageButton to={'/housekeeping/catalogue/page/' + pageId + '/items'} title="Retour à la liste" icon="undo" color="secondary" />
                    </>}
                >
                    <PageBlock>
                    <Form item={{
                        name: '',
                        description: '',
                        price: 0,
                        is_hidden: false,
                        definition_id: ''
                    }} />
                    </PageBlock>
                </PageContent>

    );
}