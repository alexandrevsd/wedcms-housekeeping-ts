import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import Api from "../../../Api";
import { IUser } from "../../../interfaces/User";
import Button from "../../main/buttons/Button";
import PageButton from "../../main/buttons/PageButton";
import PageContent from "../../main/PageContent";
import List from "../../pagesComponents/user/rooms/List";


export default function Rooms() {

    const dispatch = useDispatch();

    const { userId, urlPage } = useParams();
    const pageFromUrl: number = urlPage ? parseInt(urlPage) : 1;
    const [loading, setLoading] = useState<boolean>(true);
    const [user, setUser] = useState<IUser>();
    const [previous, setPrevious] = useState<boolean>(false);
    const [next, setNext] = useState<boolean>(false);
    const [page, setPage] = useState<number>(pageFromUrl);
    const [maxPage, setMaxPage] = useState<number>(1);

    useEffect(() => {
        loadUser();
    }, []);

    useEffect(() => {
        const pageFromUrl: number = urlPage ? parseInt(urlPage) : 1;
        if(pageFromUrl !== page) {
            setPage(pageFromUrl);
        }
    }, [urlPage]);

    const loadUser = async () => {
        const res = await Api.get('user?id=' + userId);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.user) {
            setUser(res.user);
        }
        setLoading(false);
    }

    return !loading && user ?
            <PageContent
                table={true}
                title={"Appartements de " + user.username}
                buttons={<>
                    <PageButton to={'/housekeeping/user/' + user.id} icon="user" title="Profil de l'utilisateur" color="primary" />
                    <PageButton to={'/housekeeping/user/' + user.id + '/hand'} title="Voir sa main" icon="hand-holding" color="primary" />
                </>}
            >
                <List page={page} setPage={setPage} setMaxPage={setMaxPage} setPrevious={setPrevious} setNext={setNext} user={user} />
                <div className="px-4 text-center row">
                    <div className="col">
                        <Button to={'/housekeeping/user/' + user.id + '/rooms/' + (page - 1)} color="secondary" icon="arrow-left" title="Page précédente" disabled={!previous} />
                    </div>
                    <div className="col align-self-center">
                        Page {page}/{maxPage}
                    </div>
                    <div className="col">
                        <Button to={'/housekeeping/user/' + user.id + '/rooms/' + (page + 1)} color="secondary" icon="arrow-right" title="Page suivante" disabled={!next} />
                    </div>
                </div>
            </PageContent>
            :
            <PageContent title="Appartements en chargement">
                <p>Chargement...</p>
            </PageContent>

}