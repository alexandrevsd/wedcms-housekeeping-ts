import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useParams } from "react-router-dom";
import Api from "../../../Api";
import { IUser } from "../../../interfaces/User";
import { userSelectors } from "../../../store/userSelectors";
import PageButton from "../../main/buttons/PageButton";
import Input from "../../main/form/Input";
import Select from "../../main/form/Select";
import SubmitButton from "../../main/form/SubmitButton";
import PageContent from "../../main/PageContent";
import PageBlock from "../../main/pageContent/PageBlock";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export default function Ban() {

    const { userId } = useParams();

    const userState = useSelector(userSelectors);
    const dispatch = useDispatch();

    const [loading, setLoading] = useState<boolean>(true);
    const [submited, setSubmited] = useState<boolean>(false);
    const [locked, setLocked] = useState<boolean>(true);
    const [user, setUser] = useState<IUser>();

    const [inputMessage, setInputMessage] = useState<string>();
    const handleInputMessage = (e: any) => setInputMessage(e.target.value);
    const [inputLength, setInputLength] = useState<number>(2);
    const handleInputLength = (e: any) => setInputLength(e.target.value);

    useEffect(() => {
        loadUser();
    }, []);

    const loadUser = async () => {
        const res = await Api.get('user?id=' + userId);
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.user) {
            setUser(res.user);
            if(userState.user.rank > res.user.rank || userState.user.rank === 7) {
                setLocked(false);
            }
        }
        setLoading(false);
    }

    const handleSubmitBan = async (e: any) => {
        e.preventDefault();
        e.target.disabled = true;
        const res = await Api.post('user/ban', {id: user?.id, message: inputMessage, length: inputLength});
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.response === 200) {
            const date = new Date((Date.now()) + (inputLength * 60 * 60 * 1000));
            toast.success("L'utilisateur a bien été banni jusqu'au " + date.toLocaleString())
            setSubmited(true);
        }
        e.target.disabled = false;
    }

    return (
        loading ?
            <h1>Chargement...</h1>
        :
            user && !submited && !locked ?
            <PageContent
                title={"Bannir " + user.username}
                buttons={<>
                    <PageButton to={'/housekeeping/user/' + user.id} title="Profil de l'utilisateur" icon="user" color="primary" />
                </>}
            >
                <PageBlock>
                    <form>
                        <Input label="Raison du bannissement" value={inputMessage} onChange={handleInputMessage} type="text" id="message" />
                        <Select label="Durée du bannissement" value={inputLength} onChange={handleInputLength}>
                            <option value="2">2 Heures</option>
                            <option value="4">4 Heures</option>
                            <option value="12">12 Heures</option>
                            <option value="24">24 Heures</option>
                            <option value="48">2 Jours</option>
                            <option value="72">3 Jours</option>
                            <option value="168">7 Jours</option>
                            <option value="336">14 Jours</option>
                            <option value="504">21 Jours</option>
                            <option value="720">30 Jours</option>
                            <option value="1440">60 Jours</option>
                            <option value="8760">365 Jours</option>
                            <option value="17520">730 Jours</option>
                            <option value="100008">4167 Jours</option>
                        </Select>
                        <SubmitButton onClick={handleSubmitBan} color="danger">Bannir</SubmitButton>
                    </form>
                </PageBlock>
            </PageContent>
            :
                submited && user ?
                    <Navigate to={"/housekeeping/user/" + user.id} />
                :
                    <Navigate to={"/housekeeping/users"} />

    );
}