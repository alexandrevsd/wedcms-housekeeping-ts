import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Navigate, useParams } from "react-router-dom";
import Api from "../../../Api";
import { IUser } from "../../../interfaces/User";
import PageButton from "../../main/buttons/PageButton";
import PageContent from "../../main/PageContent";
import List from "../../pagesComponents/user/hand/List";
import Button from "../../main/buttons/Button";

export default function Hand() {

    const dispatch = useDispatch();

    const { urlPage, userId } = useParams();
    const pageFromUrl: number = urlPage ? parseInt(urlPage) : 1;
    const [previous, setPrevious] = useState<boolean>(false);
    const [next, setNext] = useState<boolean>(false);
    const [page, setPage] = useState<number>(pageFromUrl);
    const [maxPage, setMaxPage] = useState<number>(1);
    const [loading, setLoading] = useState<boolean>(true);
    const [user, setUser] = useState<IUser>();

    useEffect(() => {
        loadUser();
    }, []);

    useEffect(() => {
        const pageFromUrl: number = urlPage ? parseInt(urlPage) : 1;
        if (pageFromUrl !== page) {
            setPage(pageFromUrl);
        }
    }, [urlPage]);

    const loadUser = async () => {
        const res = await Api.get('user?id=' + userId);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.user) {
            setUser(res.user);
        }
        setLoading(false);
    }

    return (
        loading ?
            <p>Chargement en cours...</p>
        :
            user ?
                <PageContent
                    title={"Main de " + user.username}
                    buttons={<>
                        <PageButton to={'/housekeeping/user/' + user.id} title="Profil de l'utilisateur" icon="user" color="primary" />
                        <PageButton to={'/housekeeping/user/' + user.id + '/rooms'} title="Voir ses appartements" icon="building" color="primary" />
                    </>}
                >
                    <List user={user} page={page} setPage={setPage} setPrevious={setPrevious} setNext={setNext} setMaxPage={setMaxPage} />
                    <div className="px-4 text-center row">
                        <div className="col">
                            <Button to={'/housekeeping/user/' + user.id + '/hand/' + (page - 1)} color="secondary" icon="arrow-left" title="Page précédente" disabled={!previous} />
                        </div>
                        <div className="col align-self-center">
                            Page {page}/{maxPage}
                        </div>
                        <div className="col">
                            <Button to={'/housekeeping/user/' + user.id + '/hand/' + (page + 1)} color="secondary" icon="arrow-right" title="Page suivante" disabled={!next} />
                        </div>
                    </div>
                </PageContent>
                :
                <Navigate to={"/housekeeping/users"} />

    );
}