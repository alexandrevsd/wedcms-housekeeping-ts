import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {Navigate, useLocation} from 'react-router';
import { useParams, useSearchParams } from "react-router-dom";
import Api from "../../Api";
import PageButton from "../main/buttons/PageButton";
import PageContent from "../main/PageContent";
import { IRoom } from "../../interfaces/Room";
import { enterHotelRoom } from "../../Habbo";
import Form from "../pagesComponents/room/Form";
import Rights from "../pagesComponents/room/Rights";
import Modal from "../main/Modal";
import useModal from "../../hooks/useModal";
import { toast } from "react-toastify";
import Informations from "../pagesComponents/room/Informations";

export default function RoomPage() {

    const [searchParams, setSearchParams] = useSearchParams();
    const dispatch = useDispatch();

    const { isShowing, toggle: toggleConfirm } = useModal();
    const [modalContent, setModalContent] = useState<string>('');
    const { roomId } = useParams();
    const userId: string | null = searchParams.get('userId');
    const userIdLink: string = userId ? '?userId=' + userId : '';
    const [loading, setLoading] = useState<boolean>(true);
    const [room, setRoom] = useState<IRoom>();
    const [roomDelete, setRoomDelete] = useState<boolean>(false);

    useEffect(() => {
        loadRoom();
    }, []);

    const loadRoom = async () => {
        const res = await Api.get('room?id=' + roomId);
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.room) {
            setRoom(res.room);
        }
        setLoading(false);
    }

    const handleButtonDeleteRoomConfirm = (e: any) => {
        e.preventDefault();
        if(room) {
            setModalContent("Voulez-vous vraiment supprimer l'appartement \"" + room.name + "\" ?")
            toggleConfirm();
        }
    }

    const handleButtonDeleteRoom = async () => {
        toggleConfirm();
        const res = await Api.delete('room?id=' + roomId);
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            toast.success("L'appartement a bien été supprimé, un redémarrage du serveur doit être effectué.")
            setRoomDelete(true);
        }
    }

    const handleButtonTeleportToRoom = () => enterHotelRoom(roomId);

    return (

        roomDelete ?
            userIdLink !== '' ?
                <Navigate to={"/housekeeping/user/" + userId + "/rooms"} />
            :
                <Navigate to="/housekeeping/rooms" />
            
        :
            room ?
                <PageContent
                    title={"Appartement \"" + room.name + "\""}
                    buttons={<>
                        <PageButton to={'/housekeeping/room/' + room.id + '/items' + userIdLink} title="Objets dans l'appartement" icon="box-open" color="primary" />
                        <PageButton to={'/housekeeping/room/' + room.id + '/logs' + userIdLink} title="Logs du chat" icon="comment" color="primary" />
                        <PageButton onClick={handleButtonTeleportToRoom} title="Se téléporter dedans" icon="sign-in-alt" color="primary" />
                        <PageButton onClick={handleButtonDeleteRoomConfirm} title="Supprimer l'appartement" icon="trash" color="danger" />
                        {userId ?
                            <PageButton to={'/housekeeping/user/' + userId + '/rooms'} title="Retour à la liste" icon="undo" color="secondary" />
                        :
                            <PageButton to={'/housekeeping/rooms'} title="Retour à la liste" icon="undo" color="secondary" />
                        }
                    </>}
                >
                    <Modal
                        title="Attention !"
                        content={modalContent}
                        isShowing={isShowing}
                        onClick={handleButtonDeleteRoom}
                        yesButton="Oui"
                        noButton="Annuler"
                        hide={toggleConfirm}
                    />
                    <Informations room={room} />
                    <Form room={room} />
                    <Rights room={room} />
                </PageContent>
                :
                <PageContent
                    title={loading ? 'Chargement' : 'Erreur'}
                >
                    {loading ? 'Chargement en cours...' : <h2>Utilisateur introuvable</h2>}
                </PageContent>

    );
}