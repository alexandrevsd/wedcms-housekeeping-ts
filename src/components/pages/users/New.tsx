import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useParams } from "react-router-dom";
import Api from "../../../Api";
import { IUser } from "../../../interfaces/User";
import { userSelectors } from "../../../store/userSelectors";
import PageButton from "../../main/buttons/PageButton";
import Input from "../../main/form/Input";
import Select from "../../main/form/Select";
import SubmitButton from "../../main/form/SubmitButton";
import PageContent from "../../main/PageContent";
import PageBlock from "../../main/pageContent/PageBlock";
import 'react-toastify/dist/ReactToastify.css';

export default function New() {

    const { userId } = useParams();

    const userState = useSelector(userSelectors);
    const dispatch = useDispatch();
    
    const [submitted, setSubmitted] = useState<boolean>(false);
    const [inputUsername, setInputUsername] = useState<string>('');
    const [inputPassword, setInputPassword] = useState<string>('');
    const [inputRank, setInputRank] = useState<string>('1');

    const handleInputUsername = (e: any) => setInputUsername(e.target.value);
    const handleInputPassword = (e: any) => setInputPassword(e.target.value);
    const handleInputRank = (e: any) => setInputRank(e.target.value);

    const handleSubmitUser = async (e: any) => {
        e.preventDefault();
        e.target.disabled = true;
        const res = await Api.post('user', {user: {username: inputUsername, password: inputPassword, rank: inputRank}});
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.response === 200) {
            setSubmitted(true);
        }
        e.target.disabled = false;
    }

    const selectOptions: Array<JSX.Element> = [];
    userState.rankNames.forEach((name: string, id: number) => {
        selectOptions.push(<option value={id} key={id}>{name}</option>)
    })

    return (
        !submitted ?
            <PageContent
                title={"Nouvel utilisateur"}
                buttons={<>
                    <PageButton to={'/housekeeping/users'} title="Liste des utilisateurs" icon="users" color="primary" />
                </>}
            >
                <PageBlock>
                    <form>
                        <Input label="Nom d'utilisateur" type="text" id="username" value={inputUsername} onChange={handleInputUsername} />
                        <Input label="Mot de passe" type="password" id="password" value={inputPassword} onChange={handleInputPassword} />
                        <Select label="Rang" value={inputRank} onChange={handleInputRank}>
                            {selectOptions}
                        </Select>
                        <SubmitButton color="success" onClick={handleSubmitUser}>Créer l'utilisateur</SubmitButton>
                    </form>
                </PageBlock>
            </PageContent>
        :
            <Navigate to={"/housekeeping/users"} />

    );
}