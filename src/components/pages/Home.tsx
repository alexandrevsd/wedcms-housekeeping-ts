import PageContent from "../main/PageContent";
import Counters from "../pagesComponents/home/Counters";
import HotelAlert from "../pagesComponents/home/HotelAlert";
import WedCmsInfos from "../pagesComponents/home/WedCmsInfos";
import HotelCommands from "../pagesComponents/home/HotelCommands";

export default function HomePage() {
    return (
        <PageContent title="Accueil">
            <Counters />
            <div className="row">
                <div className="col-lg-6 mb-4">
                    <HotelAlert />
                    <WedCmsInfos />
                </div>
                <div className="col-lg-6 mb-4">
                    <HotelCommands />
                </div>
            </div>
        </PageContent>
    );
}