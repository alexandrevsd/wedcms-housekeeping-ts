import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {Navigate} from 'react-router';
import { useParams } from "react-router-dom";
import Api from "../../Api";
import { IUser } from "../../interfaces/User";
import PageButton from "../main/buttons/PageButton";
import PageContent from "../main/PageContent";
import Vouchers from "../pagesComponents/user/Vouchers";
import Ips from "../pagesComponents/user/Ips";
import Bans from "../pagesComponents/user/Bans";
import Profile from "../pagesComponents/user/Profile";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Modal from "../main/Modal";
import useModal from "../../hooks/useModal";
import { userSelectors } from "../../store/userSelectors";
import Badges from "../pagesComponents/user/Badges";

export default function User() {

    const userState = useSelector(userSelectors);
    const dispatch = useDispatch();

    const { userId } = useParams();

    const { isShowing, toggle: toggleConfirm } = useModal();
    const [modalContent, setModalContent] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(true);
    const [deleted, setDeleted] = useState<boolean>(false);
    const [user, setUser] = useState<IUser>();
    const [ip, setIp] = useState<string>();
    const [banned, setBanned] = useState<boolean>(false);

    useEffect(() => {
        loadUser();
    }, []);

    const loadUser = async () => {
        const res = await Api.get('user?id=' + userId);
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.user) {
            setUser(res.user);
        }
        setLoading(false);
    }

    const handleButtonDeleteUserConfirm = (e: any) => {
        e.preventDefault();
        if(user) {
            setModalContent("Voulez-vous vraiment supprimer l'utilisateur \"" + user.username + "\" ?")
            toggleConfirm();
        }
    }

    const handleButtonDeleteUser = async (e: any) => {
        e.preventDefault();
        if(user) {
            const res = await Api.delete('user?id=' + user.id);
            const logged = Api.checkLogged(res, dispatch);
            if(logged && res.response === 200) {
                setDeleted(true);
                toast.success("L'utilisateur a bien été supprimé.");
            }
        }
    }

    const handleButtonUnbanUser = async (e: any) => {
        e.preventDefault();
        e.target.disabled = true;
        if(user) {
            const res = await Api.post('user/unban', {id: user.id});
            const logged = Api.checkLogged(res, dispatch);
            if(logged && res.response === 200) {
                setBanned(false);
                toast.success("L'utilisateur a bien été débanni.");
            }
        }
        e.target.disabled = false;
    }

    const locked = (
        user && (
            (userState.user.id === user.id)
            || (userState.user.rank <= user.rank)
        )
    );

    return (

        deleted ?
            <Navigate to="/housekeeping/users" />
        :
            user ?
                <PageContent
                    title={"Profil de " + user.username}
                    buttons={<>
                        <PageButton to={'/housekeeping/user/' + user.id + '/hand'} title="Voir sa main" icon="hand-holding" color="primary" />
                        <PageButton to={'/housekeeping/user/' + user.id + '/rooms'} title="Voir ses appartements" icon="building" color="primary" />
                        {banned ?
                            <PageButton onClick={handleButtonUnbanUser} title="Débannir l'utilisateur" icon="ban" color="success" />
                        :
                            !locked && <PageButton to={'/housekeeping/user/' + user.id + '/ban'} title="Bannir l'utilisateur" icon="user-slash" color="danger" />
                        }
                        {!locked && <PageButton onClick={handleButtonDeleteUserConfirm} title="Supprimer l'utilisateur" icon="trash" color="danger" />}
                    </>}
                >
                    <Modal
                        title="Attention !"
                        content={modalContent}
                        isShowing={isShowing}
                        onClick={handleButtonDeleteUser}
                        yesButton="Oui"
                        noButton="Annuler"
                        hide={toggleConfirm}
                    />
                    <Profile banned={banned} ip={ip} user={user} setUser={setUser} />
                    <Badges user={user} />
                    <Bans setBanned={setBanned} banned={banned} user={user} />
                    <Ips setIp={setIp} user={user} />
                    <Vouchers user={user} />
                </PageContent>
                :
                <PageContent
                    title={loading ? 'Chargement' : 'Erreur'}
                >
                    {loading ? 'Chargement en cours...' : <h2>Utilisateur introuvable</h2>}
                </PageContent>

    );
}