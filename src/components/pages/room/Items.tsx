import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams, useSearchParams } from "react-router-dom";

import Api from "../../../Api";
import { enterHotelRoom } from "../../../Habbo";
import { IRoom } from "../../../interfaces/Room";

import PageButton from "../../main/buttons/PageButton";
import PageContent from "../../main/PageContent";
import Button from "../../main/buttons/Button";
import List from "../../pagesComponents/room/items/List";

export default function Items() {

    const {roomId} = useParams();
    const [searchParams, setSearchParams] = useSearchParams();
    const dispatch = useDispatch();

    const { urlPage } = useParams();
    const pageFromUrl: number = urlPage ? parseInt(urlPage) : 1;
    const userId: string | null = searchParams.get('userId');
    const userIdLink: string = userId ? '?userId=' + userId : '';
    const [loading, setLoading] = useState<boolean>(true);
    const [page, setPage] = useState<number>(pageFromUrl);
    const [maxPage, setMaxPage] = useState<number>(1);
    const [previous, setPrevious] = useState<boolean>(false);
    const [next, setNext] = useState<boolean>(false);
    const [room, setRoom] = useState<IRoom>();

    useEffect(() => {
        loadRoom();
    }, []);

    useEffect(() => {
        const pageFromUrl: number = urlPage ? parseInt(urlPage) : 1;
        if (pageFromUrl !== page) {
            setPage(pageFromUrl);
        }
    }, [urlPage]);

    const loadRoom = async () => {
        const res = await Api.get('room?id=' + roomId);
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.room) {
            setRoom(res.room)
        }
        setLoading(false);
    }

    const handleButtonTeleportToRoom = () => enterHotelRoom(roomId);

    return (
        loading ?
            <p>Chargement en cours...</p>
        :
            room ?
            <>
                <PageContent
                    title={"Objets de \"" + room.name + "\""}
                    buttons={<>
                        <PageButton to={'/housekeeping/room/' + room.id + userIdLink} title="Paramètres de l'appartement" icon="cog" color="primary" />
                        <PageButton to={'/housekeeping/room/' + room.id + '/logs' + userIdLink} title="Logs du chat" icon="comment" color="primary" />
                        <PageButton onClick={handleButtonTeleportToRoom} title="Se téléporter dedans" icon="sign-in-alt" color="primary" />
                        {userId ?
                            <PageButton to={'/housekeeping/user/' + userId + '/rooms'} title="Retour à la liste" icon="undo" color="secondary" />
                        :
                            <PageButton to={'/housekeeping/rooms'} title="Retour à la liste" icon="undo" color="secondary" />
                        }
                    </>}
                >
                    <List page={page} setPage={setPage} setMaxPage={setMaxPage} setNext={setNext} setPrevious={setPrevious} room={room} />
                </PageContent>
                <div className="px-4 text-center row">
                        <div className="col">
                            <Button to={'/housekeeping/room/' + room.id + '/items/' + (page - 1) + userIdLink} color="secondary" icon="arrow-left" title="Page précédente" disabled={!previous} />
                        </div>
                        <div className="col align-self-center">
                            Page {page}/{maxPage}
                        </div>
                        <div className="col">
                            <Button to={'/housekeeping/room/' + room.id + '/items/' + (page + 1) + userIdLink} color="secondary" icon="arrow-right" title="Page suivante" disabled={!next} />
                        </div>
                    </div>
                </>
                :
                <>
                Test
                </>

    );
}