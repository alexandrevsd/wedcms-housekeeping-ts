import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { DragDropContext } from 'react-beautiful-dnd';

import Api from "../../Api";
import { ICataloguePage } from "../../interfaces/Page";
import { reorder } from "../../utils";
import PageContent from "../main/PageContent";
import List from "../pagesComponents/catalogue/List";
import PageButton from "../main/buttons/PageButton";



export default function Catalogue() {

    const dispatch = useDispatch();

    const [pages, setPages] = useState<Array<ICataloguePage>>([]);

    useEffect(() => {
        loadCatalogue();
    }, []);

    const loadCatalogue = async () => {
        const res = await Api.get('catalogue');
        const logged = Api.checkLogged(res, dispatch);
        if (logged && res.response === 200) {
            setPages(res.pages);
        }
    }

    const handleDragEnd = (result: any) => {
        if (!result.destination) return;
        const items = reorder(pages, result);
        setPages(items);
        handleEditOrder(items);
    }

    const handleEditOrder = async (items: Array<ICataloguePage>) => {
        const res = await Api.post('catalogue/order', { pages: items });
        Api.checkLogged(res, dispatch);
    }

    return (
        <PageContent
            title="Catalogue"
            table={true}
            buttons={<>
                <PageButton to={'/housekeeping/catalogue/page/new'} title="Nouvelle page" icon="plus" color="success" />
            </>}
        >
            <DragDropContext
                onDragEnd={handleDragEnd}
            >
                <List pages={pages} />
            </DragDropContext>
        </PageContent>
    );
}