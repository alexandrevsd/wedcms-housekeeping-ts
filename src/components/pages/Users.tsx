import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Button from "../main/buttons/Button";
import PageButton from "../main/buttons/PageButton";
import PageContent from "../main/PageContent";
import List from "../pagesComponents/users/List";

export default function Users() {

    const {urlPage} = useParams();
    const pageFromUrl: number = urlPage ? parseInt(urlPage) : 1;
    const [previous, setPrevious] = useState<boolean>(false);
    const [next, setNext] = useState<boolean>(false);
    const [page, setPage] = useState<number>(pageFromUrl);
    const [maxPage, setMaxPage] = useState<number>(1);

    useEffect(() => {
        const pageFromUrl: number = urlPage ? parseInt(urlPage) : 1;
        if(pageFromUrl !== page) {
            setPage(pageFromUrl);
        }
    }, [urlPage]);
    

    return (
        <PageContent
            table={true}
            title="Utilisateurs"
            buttons={<>
                <PageButton to={'/housekeeping/user/new'} color="success">Nouveau</PageButton>
            </>}
        >
            <List
                page={page}
                setPage={setPage}
                setPrevious={setPrevious}
                setNext={setNext}
                setMaxPage={setMaxPage}
            />
            <div className="px-4 text-center row">
                <div className="col">
                    <Button to={'/housekeeping/users/' + (page - 1)} color="secondary" icon="arrow-left" title="Page précédente" disabled={!previous} />
                </div>
                <div className="col align-self-center">
                    Page {page}/{maxPage}
                </div>
                <div className="col">
                    <Button to={'/housekeeping/users/' + (page + 1)} color="secondary" icon="arrow-right" title="Page suivante" disabled={!next} />
                </div>
            </div>
        </PageContent>
    );
}