import PageContent from "../main/PageContent";
import PageBlock from "../main/pageContent/PageBlock";


export default function ErrorPage() {
    
    return (
        <PageContent title="Erreur 404">
            <PageBlock>
                La page demandée n'existe pas.
            </PageBlock>
        </PageContent>
    );
}