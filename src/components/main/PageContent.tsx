export default function PageContent(props: any) {
    const paddingContainer = props.table ? ' px-0' : '';
    const paddingTitle = props.table ? ' pl-4' : '';
    const paddingButtons = props.table ? ' pr-4' : '';
    return (
        <div className={"container-fluid" + paddingContainer}>
            <div className="row">
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className={"h3 mb-0 text-gray-400" + paddingTitle}>{props.title}</h1>
                    {props.buttons && <div className={"btn-toolbar mb-2 mb-md-0" + paddingButtons}>
                        {props.buttons}
                    </div>}
                </div>
                {props.children}
            </div>
        </div>
    );
}