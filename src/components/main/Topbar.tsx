import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import Api from "../../Api";
import { setLogoutAction } from "../../store/userActions";
import { userSelectors } from "../../store/userSelectors";
import TopbarContainer from "./topbar/TopbarContainer";
import TopbarLink from "./topbar/TopbarLink";

export default function Topbar(props: any) {

    const userState = useSelector(userSelectors);
    const dispatch = useDispatch();    
    
    const handleLogout = async (e: any) => {
        e.target.disabled = true;
        const res = await Api.get('auth/logout');
        const logged = Api.checkLogged(res, dispatch);
        if(logged && res.response === 200) {
            dispatch(setLogoutAction());
            toast.success("Tu es bien déconnecté !");
        }
        e.target.disabled = false;        
    }    

    return (
        <TopbarContainer
            username={userState.user.username}
            figure={userState.user.figure}
        >
            <TopbarLink icon="sign-out-alt" onClick={handleLogout}>Déconnexion</TopbarLink>
        </TopbarContainer>
    );
}