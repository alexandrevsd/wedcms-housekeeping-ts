export default function Input(props: any) {
    return (
        <div className="mb-3">
            {props.label && <label htmlFor={props.id}>{props.label}</label>}
            <input
                id={props.id}
                onChange={props.onChange}
                type={props.type}
                value={props.value}
                placeholder={props.placeholder}
                className="form-control form-control-sm wd-bg-black text-gray-300"
            />
            {props.advice && <small className="form-text text-muted">{props.advice}</small>}
        </div>
    );
}