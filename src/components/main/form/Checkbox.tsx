export default function Checkbox(props: any) {
    return (
        <div className="mb-3 form-check">
            <input type="checkbox" onChange={props.onChange} value={props.value} checked={props.checked} className="form-check-input" id={props.id} />
            <label className="form-check-label" htmlFor={props.id}>{props.label}</label>
        </div>
    );
}