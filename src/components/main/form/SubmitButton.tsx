export default function SubmitButton(props: any) {
    return <button
        type="submit"
        onClick={props.onClick}
        className={"mb-3 btn btn-sm btn-outline-" + props.color + " btn-block" + (props.className ? ' ' + props.className : '')}
    >
        {props.children}
    </button>;
}