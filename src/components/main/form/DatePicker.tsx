import DatePickerBlock from "react-datepicker";
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import fr from 'date-fns/locale/fr';
import "react-datepicker/dist/react-datepicker.css";
registerLocale('fr', fr)

export default function DatePicker(props: any) {
    return (
        <div className="mb-3">
            <label>{props.label}</label>
            <DatePickerBlock
                className="wd-datetime wd-bg-black border-1 text-gray-400"
                selected={props.selected}
                showTimeSelect
                locale={"fr"}
                dateFormat="dd/MM/yyyy HH:mm"
                onChange={(date: any) => props.onChange(date)}
            />
        </div>
    );
}