export default function Textarea(props: any) {
    return (
        <div className="mb-3">
            {props.label && <label htmlFor={props.id}>{props.label}</label>}
            <textarea
                id={props.id}
                onChange={props.onChange}
                value={props.value}
                placeholder={props.placeholder}
                className="form-control form-control-sm wd-bg-black text-gray-300"
            />
        </div>
    );
}