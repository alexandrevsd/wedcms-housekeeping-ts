import { useEffect } from "react";
import IImage from "./ImagePicker";

const ImageStyle = (width: string, height: string): any => ({
    width: 'auto',
    height: 'auto'
});

export default function ImagePickerImage(props: any) {

    const { src, isSelected, onClick, value } = props;
    
    return (
        <div className={`responsive m-1 p-0 col${isSelected ? " selected" : ""}`} onClick={onClick} id={value}>
        <img src={src}
          id={value}
          className={`p-4 thumbnail${isSelected ? " selected" : ""}`}
          style={ImageStyle('150px', '150px')}
        />
        <div className="checked">
          <div className="icon" style={{bottom:'20px'}} />
        </div>
      </div>
    );
}