import { useEffect } from "react";
import { useState } from "react"
import ImagePickerImage from "./ImagePickerImage";

export interface IImagePickerImage {
    value: string,
    src: string,
    isSelected?: boolean
}

export default function ImagePicker(props: any) {

    const {onSelect, images, selected} = props;
    const [imagesToShow, setImagesToShow] = useState<Map<string, IImagePickerImage>>(new Map());
    const [saveImages, setSaveImages] = useState<Map<string, IImagePickerImage>>(new Map());

    useEffect(() => {        
        const newImagesToShow: Map<string, IImagePickerImage> = new Map();        
        images.forEach((image: IImagePickerImage) => {
            const isSelected = selected.includes(image.value);
            newImagesToShow.set(image.value, {
                src: image.src,
                value: image.value,
                isSelected
            })
        });
        setImagesToShow(newImagesToShow);
        setSaveImages(newImagesToShow);
    }, [images]);
    

    const handleClickImage = (e: any) => {
        const imageValue = e.target.id;

        const newImagesToShow = new Map(props.multiple ? imagesToShow : saveImages);
        if(newImagesToShow.has(imageValue)) {
            const image = newImagesToShow.get(imageValue);
            if(image !== undefined) {
                image.isSelected = !image.isSelected;
                newImagesToShow.set(image.value, image);
                setImagesToShow(newImagesToShow);
            }
        }

        const selectedValues: Array<string> = [];
        newImagesToShow.forEach((image: IImagePickerImage) => {
            if(image.isSelected) {
                selectedValues.push(image.value);
            }
        });

        onSelect(selectedValues);
    }

    const imagesToDisplay = () => {
        const imagesToD: any[] = [];
        imagesToShow.forEach((image: IImagePickerImage, i: string) => {    
            imagesToD.push(<ImagePickerImage 
                  src={image.src}
                  key={image.value}
                  isSelected={image.isSelected}
                  onClick={handleClickImage}
                  value={image.value}
               />)
        })
        return imagesToD;
    }

    return (
        <div className="image_picker row pl-2">
          { imagesToDisplay() }
          <div className="clear"/>
        </div>
      )
}