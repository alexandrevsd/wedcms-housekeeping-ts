import { forwardRef } from "react";

function Select(props: any, ref: any) {
    return (
        <div className="mb-3">
            <label>{props.label}</label>
            <select ref={ref} value={props.value} onChange={props.onChange} className="form-select wd-bg-black text-gray-400">
                {props.children}
            </select>
        </div>
    );
}

export default forwardRef(Select);