import { useEffect, useState } from "react";
import { IImagePickerImage } from "./ImagePicker";

export default function ImageSinglePicker(props: any) {

    const {onSelect, selected} = props;
    const images: Array<IImagePickerImage> = props.images;

    const [selectedImage, setSelectedImage] = useState(-1);

    useEffect(() => {
        images.forEach((image: IImagePickerImage, i: number) => {
            if(image.value === selected) {
                setSelectedImage(i);
            }
        })
    }, [selected, images]);

    const handleClickImage = (e: any) => {
        const imageIndex = e.target.id;
        setSelectedImage(imageIndex);
        onSelect(images[imageIndex].value);
    }

    return (
        <div className="wd-image-picker">
            {images.map((image: IImagePickerImage, i: number) => {
                const selected = (selectedImage.toString() === i.toString());
                return (
                    <div
                        key={i}
                        onClick={handleClickImage}
                        id={i.toString()}
                        className={selected ? 'selected' : ''}
                    >
                        <img
                            src={image.src}
                            id={i.toString()}
                        />
                    </div>
                )
            })}
        </div>
    );
}