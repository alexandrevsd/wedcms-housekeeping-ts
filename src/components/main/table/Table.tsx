export default function Table(props: any) {
    return (
        <table className="table table-dark table-hover text-center">
            {props.children}
        </table>
    );
}