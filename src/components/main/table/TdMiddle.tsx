export default function TdMiddle(props: any) {
    let className = '';
    if(props.className) className = ' ' + props.className;
    return (
        <td className={'align-middle' + className}>
            {props.children}
        </td>
    );
}