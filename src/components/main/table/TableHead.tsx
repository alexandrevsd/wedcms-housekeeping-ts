export default function TableHead(props: any) {
    return (
        <thead>
            <tr>
                {props.children}
            </tr>
        </thead>
    );
}