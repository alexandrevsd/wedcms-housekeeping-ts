import { useEffect } from "react";
import SidebarContainer from "./sidebar/SidebarContainer";
import SidebarLink from "./sidebar/SidebarLink";
import SidebarTitle from "./sidebar/SidebarTitle";
import $ from "jquery";

export default function Sidebar() {

    useEffect(() => {
        loadJQueryForTheme();
    });

    return (
        <SidebarContainer>
            <SidebarLink icon="tachometer-alt" to="/housekeeping/">Accueil</SidebarLink>
            <SidebarTitle>Kepler</SidebarTitle>
            <SidebarLink icon="users" to="/housekeeping/users">Utilisateurs</SidebarLink>
            <SidebarLink icon="book" to="/housekeeping/catalogue">Catalogue</SidebarLink>
            <SidebarLink icon="gift" to="/housekeeping/vouchers">Bons d'achat</SidebarLink>
            <SidebarTitle>WedCMS</SidebarTitle>
            <SidebarLink icon="newspaper" to="/housekeeping/news">News</SidebarLink>
            <SidebarLink icon="bullhorn" to="/housekeeping/announcements">Annonces</SidebarLink>
            <SidebarLink icon="shopping-cart" to="/housekeeping/shop">Boutique</SidebarLink>
            <SidebarLink icon="question" to="/housekeeping/help">Centre d'aide</SidebarLink>
            <SidebarTitle>Paramètres</SidebarTitle>
            <SidebarLink icon="server" to="/housekeeping/settings/kepler">Kepler</SidebarLink>
            <SidebarLink icon="desktop" to="/housekeeping/settings/wedcms">WedCMS</SidebarLink>
            <SidebarLink icon="credit-card" to="/housekeeping/settings/paypal">Paypal</SidebarLink>
        </SidebarContainer>
    );
}


function loadJQueryForTheme() {

    function getWidth(): number {
        const getWindowWidth = $(window).width();
        return getWindowWidth !== undefined ? getWindowWidth : 0;
    }

    $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').toggle(false);
        }
        ;
    });

    $(window).resize(function () {
        if (getWidth() < 768) {
            $('.sidebar .collapse').toggle(false);
        }
        if (getWidth() < 480 && !$(".sidebar").hasClass("toggled")) {
            $("body").addClass("sidebar-toggled");
            $(".sidebar").addClass("toggled");
            $('.sidebar .collapse').toggle(false);
        }
        ;
    });

    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
        if (getWidth() > 768) {
            let e0: any = e.originalEvent
            let delta = e0 !== undefined ? e0.wheelDelta || -e0.detail : 0;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        }
    });

}