export default function TopbarLink(props: any) {
    return (
        <button onClick={props.onClick} className="dropdown-item">
            <i className={"fas fa-" + props.icon + " fa-sm fa-fw mr-2 text-gray-400"} aria-hidden="true"></i>
            {props.children}
        </button>
    );
}