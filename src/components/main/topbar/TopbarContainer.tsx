import { useState, useEffect, useRef } from "react";
import OutsideAlerter from "../OutsideAlerter";


export default function TopbarContainer(props: any) {

    const [dropdownToggle, setDropdownToggle] = useState('');

    const handleMenuOpen = (e: any) => {
        e.preventDefault();
        if(dropdownToggle === '') {
            setDropdownToggle(' d-block');
        } else {
            setDropdownToggle('');
        }
    }

    return (
        <nav className="navbar navbar-expand navbar-dark bg-black topbar mb-4 static-top shadow">
            <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
                <i className="fa fa-bars" aria-hidden="true"></i>
            </button>
            <ul className="navbar-nav ml-auto">
                <li className="nav-item dropdown no-arrow">
                    <a onClick={handleMenuOpen} className="nav-link dropdown-toggle" href="#">
                        <span className="mr-2 d-none d-lg-inline text-gray-600 small">{props.username}</span>
                        <div className="user-profile-image ml-1">
                            <img src={"/habbo-imager/?figure=" + props.figure + "&little=true"} />
                        </div>
                    </a>
                    <OutsideAlerter callback={() => setDropdownToggle('')}>
                        <div className={"dropdown-menu dropdown-menu-right shadow animated--grow-in" + dropdownToggle} aria-labelledby="userDropdown">
                            {props.children}
                        </div>
                    </OutsideAlerter>
                </li>
            </ul>
        </nav>
    );
}