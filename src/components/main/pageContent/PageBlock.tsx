export default function PageBlock(props: any) {
    return (
        <div className={"col-lg-12 mb-4" + (props.className && ' ' + props.className)}>        
            <div className="card shadow mb-4 text-white border-0" style={{backgroundColor: '#202020'}}>
                {props.title && <div className="card-header py-3 wd-bg-black wd-border-black">
                    <h6 className="m-0 font-weight-bold">{props.title}</h6>
                </div>}
                <div className="card-body" style={{backgroundColor: '#202020', borderRadius: '10px'}}>
                    {props.children}
                </div>
            </div>
        </div>
    );
}