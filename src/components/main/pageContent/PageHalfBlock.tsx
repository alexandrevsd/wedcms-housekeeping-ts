export default function PageHalfBlock(props: any) {
    return (
        <div className="card shadow mb-4 text-white border-0 wd-bg-black">
            <div className="card-header py-3 wd-bg-black wd-border-black">
                <h6 className="m-0 font-weight-bold">{props.title}</h6>
            </div>
            <div className="card-body wd-bg-black" style={{borderRadius: '10px'}}>
                {props.children}
            </div>
        </div>
    );
}