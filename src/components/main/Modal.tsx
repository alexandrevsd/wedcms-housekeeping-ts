import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import OutsideAlerter from "./OutsideAlerter";

const Modal = ({ isShowing, hide, title, content, buttons, onClick, ...props }: any) => {

  return (
    isShowing
    ? ReactDOM.createPortal(
      <div className={"modal" + (isShowing && " d-block")} role="dialog">
        <div className="modal-dialog" role="document">
          <OutsideAlerter callback={hide}>
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">{title}</h5>
                <button type="button" className="close" onClick={hide}>
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <p>{content}</p>
              </div>
              <div className="modal-footer">
                {buttons ?
                  buttons
                :
                <>
                  <button type="button" className="btn btn-success" onClick={onClick}>{props.yesButton || 'Yes'}</button>
                  <button type="button" className="btn btn-danger" onClick={hide}>{props.noButton || 'Cancel'}</button>  
                </>
              }
              </div>
            </div>
          </OutsideAlerter>
        </div>
      </div>
    ,
        document.body
      )
    : null
  );
}

Modal.propTypes = {
  isShowing: PropTypes.bool.isRequired,
  hide: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  buttons: PropTypes.array,
  yesButton: PropTypes.string,
  noButton: PropTypes.string,
  onClick: PropTypes.func.isRequired
};

export default Modal;