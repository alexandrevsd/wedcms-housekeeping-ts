export default function Footer(props: any) {
    return (
        <footer className="sticky-footer bg-gradient-black">
            <div className="container my-auto">
                <div className="copyright text-center my-auto">
                    <span>Copyright © WedCMS 2022</span>
                </div>
            </div>
        </footer>
    );
}