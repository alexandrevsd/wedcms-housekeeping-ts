import SidebarDivider from "./SidebarDivider";

export default function SidebarTitle(props: any) {
    return <>
        <SidebarDivider />
        <div className="sidebar-heading">{props.children}</div>
    </>;
}