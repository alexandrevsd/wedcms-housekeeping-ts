import {Link} from 'react-router-dom';

export default function SidebarLink(props: any) {
    return (
        <li className="nav-item">
            <Link aria-current="page" className="nav-link active" to={props.to}>
                <i className={"fas fa-fw fa-" + props.icon} aria-hidden="true"></i>
                <span>{props.children}</span>
            </Link>
        </li>
    );
}