import {Link} from 'react-router-dom';
import logo from '../../../images/logo.png';

export default function SidebarContainer(props: any) {
    return (
        <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <Link className="sidebar-brand d-flex align-items-center justify-content-center" to="/housekeeping">
                <div className="sidebar-brand-text mx-3"><img src={logo} alt="WedCMS"/></div>
            </Link>
            <hr className="sidebar-divider my-0" />
            {props.children}
            <hr className="sidebar-divider d-none d-md-block" />
            <div className="text-center d-none d-md-inline">
                <button className="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
    );
}