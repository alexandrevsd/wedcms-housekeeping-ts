import Footer from "./Footer";

export default function MainContent(props: any) {
    return (
        <div id="content-wrapper" className="d-flex flex-column">
            <div id="content" className="bg-gradient-black">
                {props.children}
            </div>
            <Footer />
        </div>
    );
}