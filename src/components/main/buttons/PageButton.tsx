import { Link } from "react-router-dom";

export default function PageButton(props: any) {
    const icon = props.icon && props.title ? <i className={"fas fa-fw fa-" + props.icon} title={props.title}/> : null;
    return props.onClick ?
        <button title={props.title} onClick={props.onClick} className={"btn ml-2 btn-outline-" + props.color}>
            {props.children}
            {icon}
        </button>
    :
        <Link to={props.to} title={props.title} className={"btn ml-2 btn-outline-" + props.color}>
            {props.children}
            {icon}
        </Link>
}