import { Link } from "react-router-dom";

export default function Button(props: any) {
    const disabled = props.disabled ? ' disabled' : '';
    return (
        props.to ?
            <Link
                to={props.to}
                title={props.title}
                onClick={props.onClick}
                className={"btn btn-block btn-outline-" + props.color + disabled}
            >
                {
                    props.children
                    || <i className={"fas fa-fw fa-" + props.icon} title={props.title} />
                    }
            </Link>
        :
            <button
                disabled={props.disabled}
                title={props.title}
                onClick={props.onClick}
                className={"btn btn-block btn-outline-" + props.color}
            >
                {
                    props.children
                    || <i className={"fas fa-fw fa-" + props.icon} title={props.title} />
                    }
            </button>
    );
}