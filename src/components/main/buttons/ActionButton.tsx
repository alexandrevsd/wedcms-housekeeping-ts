import { Link } from "react-router-dom";
import { IActionButtonProps } from "../../../interfaces/Props";

export default function ActionButton({onClick, title, icon, color, last, to}: IActionButtonProps) {
    let margin = ' mr-1';
    if(last) margin = '';
    return to ?
        <Link title={title} to={to} className={"btn btn-sm btn-outline-"+ color + margin} onClick={onClick}>
            <i className={"fas fa-fw fa-" + icon} title={title}/>
        </Link>
    :
        <button title={title} className={"btn btn-sm btn-outline-"+ color + margin} onClick={onClick}>
            <i className={"fas fa-fw fa-" + icon} title={title}/>
        </button>
}