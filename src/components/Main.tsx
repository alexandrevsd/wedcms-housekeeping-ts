import { Routes, Route } from 'react-router';

import MainContainer from "./main/MainContainer";
import MainContent from "./main/MainContent";
import Sidebar from "./main/Sidebar";
import Topbar from "./main/Topbar";
import Home from "./pages/Home";
import ErrorPage from "./pages/ErrorPage";
import Users from './pages/Users';
import User from './pages/User';
import Catalogue from './pages/Catalogue';
import VouchersPage from './pages/VouchersPage';
import NewsPage from './pages/NewsPage';
import AnnouncementsPage from './pages/AnnouncementsPage';
import ShopPage from './pages/ShopPage';
import HelpPage from './pages/HelpPage';
import KeplerPage from './pages/KeplerPage';
import WedCmsPage from './pages/WedCmsPage';
import PaypalPage from './pages/PaypalPage';
import { useEffect, useState } from 'react';
import Api from '../Api';
import { editRanksAction } from '../store/userActions';
import { useDispatch } from 'react-redux';
import { IRank } from '../store/userReducer';
import Ban from './pages/user/Ban';
import Hand from './pages/user/Hand';
import UserRooms from './pages/user/Rooms';
import NewUser from './pages/users/New';
import Room from './pages/Room';
import RoomItems from './pages/room/Items';
import RoomLogs from './pages/room/Logs';
import Page from './pages/catalogue/Page';
import PageItems from './pages/catalogue/PageItems';
import PageItem from './pages/catalogue/Item';


export default function Main() {

    const dispatch = useDispatch();

    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        loadRankNames();
    }, []);

    const loadRankNames = async () => {
        const res = await Api.get('misc/rankNames');
        const rankNames = new Map();
        res.rankNames.forEach((rankName: IRank) => {
            rankNames.set(rankName.id, rankName.name);
        })
        dispatch(editRanksAction(rankNames));
        setLoading(false);
    }

    return !loading ?
        <MainContainer>
            <Sidebar />
            <MainContent>
                <Topbar />
                <Routes>
                    <Route path="/housekeeping" element={<Home />} />
                    <Route path="/housekeeping/users" element={<Users />} />
                    <Route path="/housekeeping/users/:urlPage" element={<Users />} />
                    <Route path="/housekeeping/user/new" element={<NewUser />} />
                    <Route path="/housekeeping/user/:userId" element={<User />} />
                    <Route path="/housekeeping/user/:userId/ban" element={<Ban />} />
                    <Route path="/housekeeping/user/:userId/rooms" element={<UserRooms />} />
                    <Route path="/housekeeping/user/:userId/rooms/:urlPage" element={<UserRooms />} />
                    <Route path="/housekeeping/user/:userId/hand" element={<Hand />} />
                    <Route path="/housekeeping/user/:userId/hand/:urlPage" element={<Hand />} />
                    <Route path="/housekeeping/room/:roomId" element={<Room />} />
                    <Route path="/housekeeping/room/:roomId/items" element={<RoomItems />} />
                    <Route path="/housekeeping/room/:roomId/items/:urlPage" element={<RoomItems />} />
                    <Route path="/housekeeping/room/:roomId/logs" element={<RoomLogs />} />
                    <Route path="/housekeeping/room/:roomId/logs/:urlPage" element={<RoomLogs />} />
                    <Route path="/housekeeping/catalogue" element={<Catalogue />} />
                    <Route path="/housekeeping/catalogue/page/:pageId" element={<Page />} />
                    <Route path="/housekeeping/catalogue/page/:pageId/items" element={<PageItems />} />
                    <Route path="/housekeeping/catalogue/page/:pageId/items/:itemId" element={<PageItem />} />
                    <Route path="/housekeeping/vouchers" element={<VouchersPage />} />
                    <Route path="/housekeeping/news" element={<NewsPage />} />
                    <Route path="/housekeeping/announcements" element={<AnnouncementsPage />} />
                    <Route path="/housekeeping/shop" element={<ShopPage />} />
                    <Route path="/housekeeping/help" element={<HelpPage />} />
                    <Route path="/housekeeping/settings/kepler" element={<KeplerPage />} />
                    <Route path="/housekeeping/settings/wedcms" element={<WedCmsPage />} />
                    <Route path="/housekeeping/settings/paypal" element={<PaypalPage />} />
                    <Route path="*" element={<ErrorPage />} />
                </Routes>
            </MainContent>
        </MainContainer>
        :
        <h1>Chargement...</h1>
}