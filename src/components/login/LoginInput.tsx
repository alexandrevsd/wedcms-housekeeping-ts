export default function FormGroup(props: any) {
    return (
        <div className="form-group">
            <input
                onChange={props.onChange}
                value={props.value}
                type={props.type}
                name={props.name}
                className="form-control form-control-user"
                placeholder={props.placeholder}
            />
        </div>
    );
}