export default function SubmitButton(props: any) {
    return (
        <button onClick={props.onClick} type="submit" className="btn btn-primary btn-user btn-block">{props.children}</button>
    );
}