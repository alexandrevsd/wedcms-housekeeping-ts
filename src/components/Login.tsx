import {useState} from 'react';
import LoginInput from "./login/LoginInput";
import LoginForm from "./login/LoginForm";
import LoginButton from "./login/LoginButton";
import LoginContainer from "./login/LoginContainer";
import Api from '../Api';
import { useDispatch } from 'react-redux';
import { editUserAction } from '../store/userActions';
import { toast } from 'react-toastify';
import { IUser } from '../interfaces/User';

export default function Login() {

    const dispatch = useDispatch();

    const [inputUsername, setInputUsername] = useState('');
    const [inputPassword, setInputPassword] = useState('');

    const handleInputUsername = (e: any) => setInputUsername(e.target.value);
    const handleInputPassword = (e: any) => setInputPassword(e.target.value);

    const handleSubmitLogin = async (e: any) => {
        e.preventDefault();
        e.target.disabled = true;
        const res = await Api.post('auth/login', {username: inputUsername, password: inputPassword});
        if(res.user) {
            const user: IUser = res.user;
            dispatch(editUserAction(user));
        } else {
            setInputPassword('');
            toast.error('Pseudo et/ou mot de passe incorrects');
        }
        e.target.disabled = false;
    }

    return (
        <LoginContainer title="Connecte-toi !">
            <LoginForm>
                <LoginInput onChange={handleInputUsername} value={inputUsername} type="text" name="username" placeholder="Pseudo Habbo" />
                <LoginInput onChange={handleInputPassword} value={inputPassword} type="password" name="password" placeholder="Mot de passe" />
                <LoginButton onClick={handleSubmitLogin}>Se connecter</LoginButton>
            </LoginForm>
        </LoginContainer>
    );
}