import { useEffect, useState } from "react";
import {useDispatch, useSelector} from 'react-redux';
import { editUserAction } from './store/userActions';
import { userSelectors } from './store/userSelectors';
import Api from './Api';

import Main from './components/Main';
import Login from './components/Login';
import './css/sb-admin.min.css';
import './css/custom.css';
import { IUser } from "./interfaces/User";

export default function CheckLogin(props: any) {

    const userState = useSelector(userSelectors);
    const dispatch = useDispatch();

    const [loading, setLoading] = useState<boolean>(true);
    const [logged, setLogged] = useState<boolean>(false);
    
    useEffect(() => {
        const checkAuth = async () => {
            const res = await Api.get('auth/checkLogged');
            if(res.user) {
                const user: IUser = res.user;
                dispatch(editUserAction(user));
                setLogged(true);
            } else {
                setLogged(false);
            }
            setLoading(false);
        }
        checkAuth().catch(console.error);
    }, []);

    useEffect(() => {
        if(userState.logged === true && logged === false) {
            setLogged(true);
        }
        if(userState.logged === false && logged === true) {
            setLogged(false);
        }
    }, [userState]);

    return (
        loading ?
            <h1 style={{textAlign:'center',marginTop:'3em'}}>Chargement...</h1>
        :
            logged ?
                <Main />
            :
                <Login />
    );
}