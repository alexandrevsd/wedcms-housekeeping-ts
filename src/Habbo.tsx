let openedHabbo: any;

function _isHabboClient(win: any) {
    return win.location.pathname.startsWith('/client');
}

function openHabbo(url: any, forceOpen = false) {
    var win = _openHabboWindow('', 'client');
    if (!_isHabboClient(win) || forceOpen) {
        if(win !== null) win.location.href = url;
    }
    openedHabbo = win;
}

function openOrFocusHabbo(url: any) {
    if (!_isHabboOpen()) {
        openHabbo(url);
    }
    openedHabbo.focus();
}

function openAndFocusHabbo(url: any) {
    openHabbo(url, true);
    openedHabbo.focus();
}

function _openHabboWindow(url: any, target: any) {
    return window.open(url, target, 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=740,height=597');
}

function _isHabboOpen() {
    return openedHabbo !== undefined && !openedHabbo.closed;
}

function enterHotel() {
    openOrFocusHabbo("/client");
}

export function enterHotelRoom(roomId: any) {
    openAndFocusHabbo('/client?room=' + roomId);
}