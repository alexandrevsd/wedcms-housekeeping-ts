import { IUser } from "../interfaces/User";
import { USER_DELETE_ACTION, USER_UPDATE_ACTION, USER_LOGIN_ACTION, USER_LOGOUT_ACTION, RANKS_SET_ACTION, IRank } from "./userReducer";

export const editRanksAction = (rankNames: Map<number, string>) => ({
    type: RANKS_SET_ACTION,
    payload: rankNames
})

export const editUserAction = (user: IUser | null) => ({
    type: USER_UPDATE_ACTION,
    payload: user
})

export const deleteUserAction = () => ({
    type: USER_DELETE_ACTION
});

export const setLoginAction = () => ({
    type: USER_LOGIN_ACTION
});

export const setLogoutAction = () => ({
    type: USER_LOGOUT_ACTION
});