import { createStore, combineReducers } from 'redux';
import { UserReducer } from './userReducer';


export default createStore(
    combineReducers({
        user: UserReducer
    })
);