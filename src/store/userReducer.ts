import { IUser } from "../interfaces/User";

export type UserState = {
    user: IUser | null,
    logged: boolean,
    rankNames: Map<number, string>
}

export interface IRank {
  id: number,
  name: string
}

type Action = {
    type?: string,
    payload?: IUser | undefined
}

const initialState = {
  user: null,
  logged: false,
  rankNames: new Map()
};

export const USER_UPDATE_ACTION: string = 'USER_UPDATE_ACTION';
export const USER_DELETE_ACTION: string = 'USER_DELETE_ACTION';
export const USER_LOGIN_ACTION: string = 'USER_LOGIN_ACTION';
export const USER_LOGOUT_ACTION: string = 'USER_LOGOUT_ACTION';
export const RANKS_SET_ACTION: string = 'RANKS_SET_ACTION';

export function UserReducer(state: UserState = initialState, action: Action) {
    switch (action.type) {
      case RANKS_SET_ACTION:
        return {
          user: state.user,
          logged: state.logged,
          rankNames: action.payload
        };
      case USER_UPDATE_ACTION:
        return {
          user: action.payload,
          logged: true,
          rankNames: state.rankNames
        };
      case USER_DELETE_ACTION:
        return null;
      case USER_LOGIN_ACTION:
        return {
          user: state.user,
          logged: true,
          rankNames: state.rankNames
        };
      case USER_LOGOUT_ACTION:
        return {
          user: state.user,
          logged: false,
          rankNames: state.rankNames
        };
      default:
        return state;
    }
  }
