interface IgetItemNamesAndDescs {
    itemNames: Map<string, string>,
    itemDescs: Map<string, string>,
    posterNames: Map<string, string>,
    posterDescs: Map<string, string>
}

export function getItemNamesAndDescs(external_texts: string): IgetItemNamesAndDescs {
    const itemNames: Map<string, string> = new Map();
    const itemDescs: Map<string, string> = new Map();
    const posterNames: Map<string, string> = new Map();
    const posterDescs: Map<string, string> = new Map();

    const lines = external_texts.split('\r\n');
    lines.forEach(line => {
        if(line.startsWith('furni_')) {
            const decrypt = line.split('=');
            if(decrypt[0].endsWith('_name')) {
                const itemName = (decrypt[0].replace('furni_', '')).replace('_name', '');                
                itemNames.set(itemName, decrypt[1]);
            } else {
                const itemName = (decrypt[0].replace('furni_', '')).replace('_desc', '');
                itemDescs.set(itemName, decrypt[1]);
            }
        } else if(line.startsWith('poster_')) {
            const decrypt = line.split('=');
            if(decrypt[0].endsWith('_name')) {
                const itemName = (decrypt[0].replace('poster_', '')).replace('_name', '');                
                posterNames.set(itemName, decrypt[1]);
            } else {
                const itemName = (decrypt[0].replace('poster_', '')).replace('_desc', '');
                posterDescs.set(itemName, decrypt[1]);
            }
        }
    })

    return {itemNames, itemDescs, posterNames, posterDescs};
}

export const reorder = (list: Array<any>, resultOrder: any): Array<any> => {
    const { source, destination } = resultOrder;
    const startIndex = source.index;
    const endIndex = destination.index;
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};