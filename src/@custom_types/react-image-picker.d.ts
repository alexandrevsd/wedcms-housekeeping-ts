export default ImagePicker;

declare class ImagePicker extends React.Component<any, any, any> {
    constructor(props: any);
    handleImageClick(image: any): void;
    renderImage(image: any, i: any): JSX.Element;
}

declare namespace ImagePicker {
    namespace propTypes {
        const images: PropTypes.Requireable<any[]>;
        const multiple: PropTypes.Requireable<boolean>;
        const onPick: PropTypes.Requireable<(...args: any[]) => any>;
    }
}

import React from "react";
import PropTypes from "prop-types";